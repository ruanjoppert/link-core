/**
 * Get default NodeID (computerName)
 *
 * @returns
 */
export declare const getNodeID: () => string;
