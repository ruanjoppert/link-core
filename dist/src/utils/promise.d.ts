export declare class DeferredPromise {
    private promise;
    resolve: Function;
    reject: Function;
    then: Function;
    catch: Function;
    constructor();
}
