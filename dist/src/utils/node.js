"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var os_1 = __importDefault(require("os"));
/**
 * Get default NodeID (computerName)
 *
 * @returns
 */
exports.getNodeID = function () { return os_1.default.hostname().toLowerCase() + "-" + process.pid; };
