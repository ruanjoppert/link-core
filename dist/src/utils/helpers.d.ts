/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export declare const isObject: (item: object) => boolean;
/**
 * Camelize a string
 * @param str
 */
export declare const camalize: (str: string) => string;
/**
 * Rurns a deep object into a shallow
 * @param object
 * @param name
 */
export declare const shallow: <T extends object>(object: T, name?: string | object | undefined) => T;
/**
 * Delay for Promises
 *
 * @param {any} ms
 * @returns
 */
export declare const delay: (ms: number) => Promise<unknown>;
/**
 * Slipt an array in multiple array with N size
 * @param {*} array
 * @param {*} size
 */
export declare const chunkArray: <T extends any[]>(array: T, size: number) => any;
/**
 * Transform a array of key value array into object
 * @param entries
 */
export declare const fromEntries: <T>(entries: [any, any][]) => any;
/**
 * Merge two object into one deeply
 * @param target
 * @param source
 */
export declare function mergeObject(target: any, source: any): any;
/**
 * Compare values into array and return a index ordered
 * @param array
 * @param value
 * @param comparator
 */
export declare const lowerBound: <T>(array: readonly T[], value: T, comparator: (a: T, b: T) => number) => number;
export declare const parseVersion: (versionString: string) => {
    major: string;
    minor: string;
    patch: string;
};
export declare const compareVersion: (target: any, version: any) => boolean;
