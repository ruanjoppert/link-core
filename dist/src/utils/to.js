"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Short hand for errors
 * @param {function} fn Function to be handler
 * @returns {any}
 */
exports.to = function (fn) {
    if (fn && typeof fn.then === 'function') {
        return fn
            .then(function (data) { return ([null, data]); })
            .catch(function (error) { return [error, null]; });
    }
    if (fn instanceof Error)
        return [fn, null];
    // If fn is a function
    if (typeof fn === 'function') {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            try {
                return [null, fn.apply(void 0, __spread(args))];
            }
            catch (error) {
                return [error, null];
            }
        };
    }
    return [null, fn];
};
