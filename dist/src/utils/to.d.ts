/**
 * Short hand for errors
 * @param {function} fn Function to be handler
 * @returns {any}
 */
export declare const to: (fn: any) => any;
