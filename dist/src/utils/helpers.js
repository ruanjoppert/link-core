"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
exports.isObject = function (item) { return (item && typeof item === 'object' && !Array.isArray(item) && !Buffer.isBuffer(item)); };
/**
 * Camelize a string
 * @param str
 */
exports.camalize = function (str) { return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, function (m, chr) { return chr.toUpperCase(); }); };
/**
 * Rurns a deep object into a shallow
 * @param object
 * @param name
 */
exports.shallow = function (object, name) {
    if (!exports.isObject(object)) {
        return object;
    }
    return Object.keys(object).reduce(function (prev, key) {
        var _a;
        var value = object[key];
        if (exports.isObject(value)) {
            var arrName_1 = name ? __spread(name, [key]) : [key];
            var teste = exports.shallow(value, arrName_1);
            return Object.assign(prev, teste);
        }
        var arrName = name ? __spread(name, [key]) : [key];
        var keyName = arrName.length > 1 ? exports.camalize(arrName.join(' ')) : arrName.join(' ');
        return Object.assign(prev, (_a = {}, _a[keyName] = value, _a));
    }, {});
};
/**
 * Delay for Promises
 *
 * @param {any} ms
 * @returns
 */
exports.delay = function (ms) { return new Promise(function (resolve) { return setTimeout(resolve, ms); }); };
/**
 * Slipt an array in multiple array with N size
 * @param {*} array
 * @param {*} size
 */
exports.chunkArray = function (array, size) { return array.reduce(function (all, one, i) {
    var ch = Math.floor(i / size);
    all[ch] = [].concat((all[ch] || []), one);
    return all;
}, []); };
/**
 * Transform a array of key value array into object
 * @param entries
 */
exports.fromEntries = function (entries) {
    return entries.reduce(function (acc, _a) {
        var _b;
        var _c = __read(_a, 2), key = _c[0], value = _c[1];
        return (__assign(__assign({}, acc), (_b = {}, _b[key] = value, _b)));
    }, {});
};
/**
 * Merge two object into one deeply
 * @param target
 * @param source
 */
function mergeObject(target, source) {
    if (!exports.isObject(target) || !exports.isObject(source)) {
        return source;
    }
    Object.keys(source).forEach(function (key) {
        var targetValue = target[key];
        var sourceValue = source[key];
        if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
            target[key] = targetValue.concat(sourceValue);
        }
        else if (exports.isObject(targetValue) && exports.isObject(sourceValue)) {
            target[key] = mergeObject(__assign({}, targetValue), sourceValue);
        }
        else {
            target[key] = sourceValue;
        }
    });
    return target;
}
exports.mergeObject = mergeObject;
/**
 * Compare values into array and return a index ordered
 * @param array
 * @param value
 * @param comparator
 */
exports.lowerBound = function (array, value, comparator) {
    var first = 0;
    var count = array.length;
    while (count > 0) {
        var step = (count / 2) | 0;
        var it_1 = first + step;
        if (comparator(array[it_1], value) <= 0) {
            first = ++it_1;
            count -= step + 1;
        }
        else {
            count = step;
        }
    }
    return first;
};
exports.parseVersion = function (versionString) {
    var _a = __read(versionString.split('.'), 3), major = _a[0], minor = _a[1], patch = _a[2];
    return {
        major: major || '',
        minor: minor || '',
        patch: patch || ''
    };
};
exports.compareVersion = function (target, version) {
    return Object.keys(target).reduce(function (prev, key) {
        var targetValue = target[key];
        var versionValue = version[key];
        if (prev && targetValue && targetValue !== '') {
            return targetValue === versionValue;
        }
        return prev;
    }, true);
};
