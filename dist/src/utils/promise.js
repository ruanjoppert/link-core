"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DeferredPromise = /** @class */ (function () {
    function DeferredPromise() {
        var _this = this;
        this.promise = new Promise(function (resolve, reject) {
            _this.resolve = resolve;
            _this.reject = reject;
        });
        // bind `then` and `catch` to implement the same interface as Promise
        this.then = this.promise.then.bind(this.promise);
        this.catch = this.promise.catch.bind(this.promise);
        // this[Symbol.toStringTag] = 'Promise'
    }
    return DeferredPromise;
}());
exports.DeferredPromise = DeferredPromise;
