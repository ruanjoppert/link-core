"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = require("winston");
var logError = winston_1.format(function (info) {
    if (info.error && info.error.stack) {
        info.error = {
            message: info.error.message,
            stack: info.error.stack
        };
    }
    return info;
});
exports.logger = winston_1.createLogger({
    level: 'info',
    format: winston_1.format.combine(winston_1.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }), logError(), winston_1.format.json()),
    transports: [
        new winston_1.transports.File({ filename: 'logs/error.log', level: 'error' }),
        new winston_1.transports.File({ filename: 'logs/combined.log' })
    ]
});
var emoji = {
    error: '   ⊗  ',
    started: '    ☀  ',
    endpoint: '    ✒  ',
    invalid: '    ✖  ',
    messageSend: '   ✉ ↦',
    messageReceive: ' ↤ ✉  ',
    criticalError: '    ☠️  ',
    callSuccess: '    ✔  ',
    callTimeout: '    ☢  ',
    task: '    ⚒  '
};
var simpleFormat = function (info) { return info.timestamp + " " + info.level + " " + (emoji[info.label] || ' ') + " " + info.message + " (" + info.ms + ")"; };
exports.logger.add(new winston_1.transports.File({
    filename: 'logs/simple.log',
    format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.ms(), winston_1.format.printf(simpleFormat))
}));
exports.logger.add(new winston_1.transports.Console({
    format: winston_1.format.combine(winston_1.format.timestamp({
        format: 'HH:mm:ss'
    }), winston_1.format.simple(), winston_1.format.ms(), winston_1.format.colorize(), winston_1.format.align(), winston_1.format.printf(simpleFormat))
}));
