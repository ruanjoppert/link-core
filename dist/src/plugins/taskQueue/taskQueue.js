"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../../utils");
var nextTimeFunction = function (attempts, maxAttempts, delay, critical) { return attempts * ((critical - delay) / maxAttempts) + delay; };
/**
 * Default options
 */
var options = {
    attempts: 5,
    critical: 60 * 60 * 1000,
    delay: 10 * 1000,
    nextTimeFunction: nextTimeFunction
};
var TaskQueue = /** @class */ (function () {
    function TaskQueue(broker) {
        this.tasks = [];
        this.broker = broker;
    }
    /**
     * Create a new task
     */
    TaskQueue.prototype.create = function (message, config, flow, reply) {
        if (config === void 0) { config = {}; }
        var _a;
        if (!message) {
            return false;
        }
        var task = {};
        var id = ((_a = message === null || message === void 0 ? void 0 : message.head) === null || _a === void 0 ? void 0 : _a.id) || Date.now();
        task.options = utils_1.mergeObject(options, config);
        var exists = this.find(id);
        if (exists) {
            return exists;
        }
        var _b = task.options, attempts = _b.attempts, critical = _b.critical, delay = _b.delay, nextTimeFunction = _b.nextTimeFunction;
        task.id = id;
        task.attempts = 0;
        task.message = message;
        task.critical = critical;
        task.flow = flow;
        task.retry = nextTimeFunction(0, attempts, delay, critical);
        task.created = Date.now();
        task.reply = reply;
        this.broker.logger.info("Task created (" + task.id + ")", { label: 'task', task: task });
        this.tasks.push(task);
    };
    /**
     * Increase attempts
     */
    TaskQueue.prototype.increase = function (id) {
        var task = this.find(id);
        if (!task) {
            return;
        }
        var _a = task.options, attempts = _a.attempts, critical = _a.critical, delay = _a.delay, nextTimeFunction = _a.nextTimeFunction;
        task.attempts++;
        task.retry = nextTimeFunction(task.attempts, attempts, delay, critical);
    };
    /**
     * Remove a task
     */
    TaskQueue.prototype.remove = function (id) {
        var task = this.find(id);
        var index = this.tasks.indexOf(task);
        if (index > -1) {
            this.tasks.splice(index, 1);
        }
    };
    /**
     * Find task
     */
    TaskQueue.prototype.find = function (id) {
        return this.tasks.find(function (task) { return task.id === id; });
    };
    /**
     * Ends the message with success
     */
    TaskQueue.prototype.done = function (id) {
        var task = this.find(id);
        if (task.flow && task.flow.onSuccess) {
            task.flow.onSuccess(task, task.reply);
        }
        if (task.reply.end) {
            task.reply.end();
        }
        this.broker.logger.info("Task completed after " + task.attempts, { label: 'task', task: task });
        this.remove(id);
    };
    /**
     * Ends the message with error
     */
    TaskQueue.prototype.fail = function (id) {
        var task = this.find(id);
        if (task.flow && task.flow.onFail) {
            task.flow.onFail(task, task.reply);
        }
        if (task.reply.end) {
            task.reply.end();
        }
        this.broker.logger.info("Task fail after " + task.attempts, { label: 'task', task: task });
        this.remove(id);
    };
    // ==========================================================================
    /**
     * Start the queue
     */
    TaskQueue.prototype.start = function (interval) {
        var _this = this;
        if (interval === void 0) { interval = 30; }
        var start = function () {
            for (var key in _this.tasks) {
                var task = _this.tasks[key];
                _this.work(task);
            }
        };
        setInterval(function () { return start(); }, interval * 1000);
    };
    /**
     * Work on task
     */
    TaskQueue.prototype.work = function (task) {
        var _this = this;
        if (Date.now() <= task.created + task.retry) {
            return;
        }
        var attempts = task.options.attempts;
        var reply = task.reply;
        var queueReply = Object.assign(Object.create(Object.getPrototypeOf(reply)), reply);
        queueReply.enqueue = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (task.flow && task.flow.onError) {
                    task.flow.onError(task, reply);
                }
                this.increase(task.id);
                return [2 /*return*/];
            });
        }); };
        queueReply.error = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, this.fail(task.id)];
        }); }); };
        queueReply.send = function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, this.done(task.id)];
        }); }); };
        task.flow.handler(task.message, queueReply);
        if (task.attempts >= attempts) {
            this.fail(task.id);
        }
    };
    return TaskQueue;
}());
exports.TaskQueue = TaskQueue;
