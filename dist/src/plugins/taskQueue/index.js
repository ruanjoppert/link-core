"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var taskQueue_1 = require("./taskQueue");
var reply_1 = require("../../packages/reply");
var utils_1 = require("../../utils");
/**
 * Default options
 */
var defaultOptions = {
    interval: 30
};
exports.taskQueue = function (config) {
    if (config === void 0) { config = {}; }
    return function (broker) {
        var taskQueue = new taskQueue_1.TaskQueue(broker);
        var options = utils_1.mergeObject(defaultOptions, config);
        broker.taskQueue = taskQueue;
        broker.hooks.register('callResource', function (_a, next) {
            var handler = _a.handler, inbox = _a.inbox, data = _a.data;
            return __awaiter(void 0, void 0, void 0, function () {
                var id, stream, group, ack, reply_2;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            if (data.message.type !== 'REQ') {
                                next();
                                return [2 /*return*/, handler()];
                            }
                            id = data.message.head.id;
                            stream = inbox.stream, group = inbox.group;
                            if (!inbox.resource) return [3 /*break*/, 2];
                            ack = function () { return broker.transporter.ack(stream, group, id); };
                            reply_2 = new reply_1.Reply(broker, data.message, ack);
                            /**
                             * Add enqueue method
                             */
                            reply_2.enqueue = function (response) {
                                var _a, _b, _c, _d;
                                if (response) {
                                    reply_2.send(response, false);
                                }
                                var messageTaskOptions = ((_b = (_a = data.message.body) === null || _a === void 0 ? void 0 : _a.options) === null || _b === void 0 ? void 0 : _b.queue) || {};
                                var serviceTaskOptions = ((_d = (_c = inbox.resource) === null || _c === void 0 ? void 0 : _c.config) === null || _d === void 0 ? void 0 : _d.queue) || {};
                                var taskOptions = utils_1.mergeObject(serviceTaskOptions, messageTaskOptions);
                                taskQueue.create(data.message, taskOptions, inbox.resource, reply_2);
                            };
                            return [4 /*yield*/, inbox.resource.handler(data.message, reply_2)];
                        case 1:
                            _b.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            broker.transporter.ack(stream, group, data.message.head.id);
                            _b.label = 3;
                        case 3:
                            next();
                            return [2 /*return*/];
                    }
                });
            });
        });
        broker.hooks.register('afterStart', function (data, next) { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                taskQueue.start(options.interval || 30);
                broker.logger.info("Plugin TaskQueue started (" + options.interval + ")", { label: 'task' });
                next();
                return [2 /*return*/];
            });
        }); });
    };
};
