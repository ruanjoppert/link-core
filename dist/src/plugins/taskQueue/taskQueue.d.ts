export declare class TaskQueue {
    tasks: any[];
    private broker;
    constructor(broker: any);
    /**
     * Create a new task
     */
    create(message: any, config: any, flow: any, reply: any): any;
    /**
     * Increase attempts
     */
    increase(id: string): void;
    /**
     * Remove a task
     */
    remove(id: string): void;
    /**
     * Find task
     */
    find(id: string): any;
    /**
     * Ends the message with success
     */
    done(id: string): void;
    /**
     * Ends the message with error
     */
    fail(id: string): void;
    /**
     * Start the queue
     */
    start(interval?: number): void;
    /**
     * Work on task
     */
    work(task: any): void;
}
