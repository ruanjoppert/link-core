import { Packages as TransporterPackages } from '../../packages/transporter';
import { IBrokerOptions } from './broker.interface';
import { Catalog } from '../../packages/catalog';
import { nextType, endType } from '../../packages/middleware';
import { Hooks } from '../../packages/hooks';
import { Endpoint } from '../../packages/endpoint';
import { IMessageType, IMessageBody, IMessageHead, IMessage } from '../../packages/message';
export declare class Broker {
    [index: string]: any;
    options: IBrokerOptions;
    transporter: InstanceType<TransporterPackages[this['options']['transporter']['adapter']]>;
    private serializer;
    catalog: Catalog;
    hooks: Hooks;
    private inboxes;
    private middlewares;
    logger: import("winston").Logger;
    constructor(config?: Partial<IBrokerOptions>);
    /**
     * Register an endpoint to listen for new messages
     * @param endpoint
     * @param resource
     */
    listen(endpoint: Endpoint, options?: {
        group?: string;
        consumer?: string;
        initialId?: string;
    }, resource?: any): Promise<undefined>;
    /**
     * Publish a message into a channel
     * @param type message type
     * @param target stream or target string
     * @param body body
     */
    publish<T extends IMessageType>(type: T, target: string, body: IMessageBody<T>, partialHead?: Partial<IMessageHead>): Promise<string | Error>;
    /**
     * Send request and wait a response
     * @param to
     * @param params
     * @param options
     */
    call(target: string, params?: any, options?: any): Promise<IMessage | Error>;
    /**
     * Request
     */
    req(type: "REQ" | "RES" | "INFO" | "EVENT" | "CMD" | undefined, target: string, data?: any, options?: any): Promise<IMessage | Error>;
    /**
     * Start the broker
     */
    start(): Promise<Error | undefined>;
    /**
     * Register an endpoint array or a service into broker
     */
    register(endpointOrService: any): Promise<boolean | undefined>;
    /**
     * Add middleware function
     */
    use(middlewares: {
        [index: string]: (data: any, next: nextType, end: endType) => void;
    }): void;
    /**
     * Extend broker and allow build plugins
     */
    extend(plugin: any): void;
    /**
     * Send initial info message to all brokers and register external endpoints
     * @param broker
     */
    static scoutInfo(broker: Broker): Promise<void>;
    /**
     * Register commands
     * @param broker
     */
    registerCommands(): void;
    /**
     * Middleare responsable for intercepting response messages and resolving pending requests
     * @param broker
     */
    static middlewareResolvePendingRequest(broker: Broker): void;
    /**
     * Middleare responsable for intercepting CMD messages and run
     * @param broker
     */
    static middlewareCommandMessage(broker: Broker): void;
}
