import { Packages as TransporterPackages } from '../../packages/transporter';
import { Packages as SerializerPackages } from '../../packages/serializer';
export interface IBrokerOptions {
    namespace: string;
    nodeId: string;
    transporter: {
        adapter: keyof TransporterPackages;
        options?: {
            port?: any;
            host?: any;
            password?: any;
        };
    };
    serializer: {
        format: keyof SerializerPackages;
        force: boolean;
    };
    plugins?: string[];
    logLevel: 'emerg' | 'alert' | 'crit' | 'error' | 'warning' | 'notice' | 'info' | 'debug';
}
export interface Inbox {
    transporter: InstanceType<TransporterPackages['Redis']>;
    subscribe: any;
    resource?: any;
    stream: string;
    group: string;
    consumer: string;
}
