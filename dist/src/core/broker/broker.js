"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var serializer_1 = require("../../packages/serializer");
var transporter_1 = require("../../packages/transporter");
var utils_1 = require("../../utils");
var catalog_1 = require("../../packages/catalog");
var middleware_1 = require("../../packages/middleware");
var hooks_1 = require("../../packages/hooks");
var reply_1 = require("../../packages/reply");
var message_1 = require("../../packages/message");
var validator_1 = require("../../packages/validator");
var logger_1 = require("../../common/logger");
/**
 * Default options
 */
var options = {
    namespace: '',
    nodeId: utils_1.getNodeID(),
    transporter: {
        adapter: 'Redis',
        options: {
            host: '127.0.0.1',
            port: 6379
        }
    },
    serializer: {
        format: 'JSON',
        force: false
    },
    logLevel: 'info'
};
var Broker = /** @class */ (function () {
    function Broker(config) {
        if (config === void 0) { config = {}; }
        this.inboxes = [];
        this.middlewares = {};
        this.logger = logger_1.logger;
        this.options = utils_1.mergeObject(options, config);
        /**
         * Resolves adapters
         */
        this.transporter = transporter_1.transporter(this.options.transporter.adapter, this.options.transporter.options);
        this.serializer = serializer_1.serializer(this.options.serializer.format);
        /**
         * Packages
         */
        this.catalog = new catalog_1.Catalog();
        this.hooks = new hooks_1.Hooks();
        /**
         * Default middlewares
         */
        this.middlewares.receiving = new middleware_1.Middleware();
        this.middlewares.sending = new middleware_1.Middleware();
        this.middlewares.errorOnReceiving = new middleware_1.Middleware();
        this.middlewares.errorOnSending = new middleware_1.Middleware();
        this.logger.level = this.options.logLevel;
    }
    /**
     * Register an endpoint to listen for new messages
     * @param endpoint
     * @param resource
     */
    Broker.prototype.listen = function (endpoint, options, resource) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var stream, group, consumer, initialId, v, validate, subscribe, inbox;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        stream = this.options.namespace + ":" + catalog_1.Catalog.mountTargetString(endpoint);
                        group = (options === null || options === void 0 ? void 0 : options.group) || stream;
                        consumer = (options === null || options === void 0 ? void 0 : options.consumer) || this.options.nodeId;
                        initialId = (options === null || options === void 0 ? void 0 : options.initialId) || '0';
                        return [4 /*yield*/, this.hooks.call('beforeStartListen', { stream: stream, endpoint: endpoint, resource: resource })];
                    case 1:
                        _c.sent();
                        this.logger.debug("Start registering a listener on " + stream, { endpoint: endpoint, resource: resource });
                        if (!(!endpoint || !((_a = endpoint.address) === null || _a === void 0 ? void 0 : _a.domain))) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.hooks.call('invalidEndpointOnListen', { endpoint: endpoint })];
                    case 2:
                        _c.sent();
                        this.logger.warn("Target " + ((_b = endpoint.address) === null || _b === void 0 ? void 0 : _b.domain) + " invalid", { label: 'invalid', endpoint: endpoint });
                        return [2 /*return*/, Promise.reject(new Error('Invalid endpoint'))];
                    case 3:
                        /**
                         * Validate props
                         */
                        if (endpoint.props && Object.keys(endpoint.props).length > 0 && endpoint.props.constructor === Object) {
                            v = new validator_1.PropsValidator();
                            validate = v.compile(endpoint.props);
                            if (validate !== true) {
                                return [2 /*return*/, Promise.reject(validate)];
                            }
                        }
                        /**
                         * Register a new endpoint
                         */
                        endpoint.nodes = [this.options.nodeId];
                        this.register(endpoint);
                        subscribe = function (inbox) { return __awaiter(_this, void 0, void 0, function () {
                            var stream, group, consumer, read, _a, error;
                            var _this = this;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        stream = inbox.stream, group = inbox.group, consumer = inbox.consumer;
                                        return [4 /*yield*/, inbox.transporter.createStream(stream, group)];
                                    case 1:
                                        if (!((_b.sent()) !== true)) return [3 /*break*/, 3];
                                        return [4 /*yield*/, this.hooks.call('errorOnCreateStream', { stream: stream, group: group })];
                                    case 2:
                                        _b.sent();
                                        this.logger.error("Error on create stream :" + stream + " group: " + group, { label: 'error', stream: stream, group: group });
                                        return [2 /*return*/, Promise.reject('Impossible create stream')];
                                    case 3:
                                        read = function () { return __awaiter(_this, void 0, void 0, function () {
                                            var _a, errorOnRead, preMessage, _b, errorOnProcess, data, _c, errorMiddleware;
                                            var _this = this;
                                            return __generator(this, function (_d) {
                                                switch (_d.label) {
                                                    case 0: return [4 /*yield*/, utils_1.to(inbox.transporter.read(stream, consumer, group))
                                                        /**
                                                         * If an error occurs when read a message
                                                         */
                                                    ];
                                                    case 1:
                                                        _a = __read.apply(void 0, [_d.sent()
                                                            /**
                                                             * If an error occurs when read a message
                                                             */
                                                            , 2]), errorOnRead = _a[0], preMessage = _a[1];
                                                        /**
                                                         * If an error occurs when read a message
                                                         */
                                                        if (errorOnRead) {
                                                            return [2 /*return*/, Promise.reject(errorOnRead)];
                                                        }
                                                        /**
                                                         * Ignore messages sent by this same broker
                                                         */
                                                        if (preMessage.head.sender === this.options.namespace + ":" + this.options.nodeId) {
                                                            this.transporter.ack(stream, group, preMessage.head.id);
                                                            return [2 /*return*/, read()];
                                                        }
                                                        /**
                                                         * Deserialize message
                                                         */
                                                        preMessage = this.serializer.deserialize(preMessage);
                                                        return [4 /*yield*/, utils_1.to(this.middlewares.receiving.process({ message: preMessage, stream: stream, group: group }))
                                                            /**
                                                             * Error handling
                                                             */
                                                        ];
                                                    case 2:
                                                        _b = __read.apply(void 0, [_d.sent()
                                                            /**
                                                             * Error handling
                                                             */
                                                            , 2]), errorOnProcess = _b[0], data = _b[1];
                                                        if (!errorOnProcess) return [3 /*break*/, 4];
                                                        return [4 /*yield*/, utils_1.to(this.middlewares.errorOnReceiving.process(errorOnProcess))];
                                                    case 3:
                                                        _c = __read.apply(void 0, [_d.sent(), 1]), errorMiddleware = _c[0];
                                                        return [2 /*return*/, Promise.reject(errorMiddleware)];
                                                    case 4: return [4 /*yield*/, this.hooks.call('afterReceiveMessage', { message: data.message })];
                                                    case 5:
                                                        _d.sent();
                                                        this.logger.debug(data.message.type + " message receive from " + data.message.head.sender, { msg: data.message, label: 'messageReceive' });
                                                        /**
                                                         * Call the handler callback
                                                         */
                                                        return [4 /*yield*/, this.hooks.call('callResource', { inbox: inbox, data: data }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                                var id_1, ack, reply;
                                                                var _this = this;
                                                                return __generator(this, function (_a) {
                                                                    switch (_a.label) {
                                                                        case 0:
                                                                            if (!inbox.resource) return [3 /*break*/, 2];
                                                                            id_1 = data.message.head.id;
                                                                            ack = function () { return _this.transporter.ack(stream, group, id_1); };
                                                                            reply = new reply_1.Reply(this, data.message, ack);
                                                                            return [4 /*yield*/, inbox.resource.handler(data.message, reply)];
                                                                        case 1:
                                                                            _a.sent();
                                                                            return [3 /*break*/, 3];
                                                                        case 2:
                                                                            this.transporter.ack(stream, group, data.message.head.id);
                                                                            _a.label = 3;
                                                                        case 3: return [2 /*return*/];
                                                                    }
                                                                });
                                                            }); })];
                                                    case 6:
                                                        /**
                                                         * Call the handler callback
                                                         */
                                                        _d.sent();
                                                        read();
                                                        return [2 /*return*/];
                                                }
                                            });
                                        }); };
                                        return [4 /*yield*/, this.hooks.call('beforeSubscribe', { stream: stream, group: group, consumer: this.options.nodeId })];
                                    case 4:
                                        _b.sent();
                                        this.logger.info("Subscribe at: " + stream, { stream: stream, group: group, consumer: consumer });
                                        _a = __read(read(), 1), error = _a[0];
                                        /**
                                         * If has an error on receive message
                                         */
                                        if (error) {
                                            return [2 /*return*/, Promise.reject('Error on read message')];
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        }); };
                        inbox = {
                            transporter: transporter_1.transporter(this.options.transporter.adapter, __assign(__assign({}, this.options.transporter.options), { initialId: initialId })),
                            resource: resource,
                            subscribe: subscribe,
                            stream: stream,
                            group: group,
                            consumer: consumer
                        };
                        this.inboxes.push(inbox);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Publish a message into a channel
     * @param type message type
     * @param target stream or target string
     * @param body body
     */
    Broker.prototype.publish = function (type, target, body, partialHead) {
        if (partialHead === void 0) { partialHead = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var exact, endpoint, v, params, schema, valid, stream, serializedBody, head, message, _a, error, data, messageId;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.transporter.isConnected) {
                            return [2 /*return*/, Promise.reject('Transporter is not connect, call start() first and wait until complete')];
                        }
                        if (!target) {
                            return [2 /*return*/, Promise.reject('Impossible to send a message to stream')];
                        }
                        exact = !target.endsWith('!');
                        /**
                         * Remove, if exists '!' from target
                         */
                        if (!exact)
                            target = target.slice(0, -1);
                        endpoint = this.catalog.find(target);
                        if (!(!endpoint && exact)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.hooks.call('sendingTargetNotFound', { target: target })];
                    case 1:
                        _b.sent();
                        this.logger.warn("Target " + target + " not found", { label: 'invalid', target: target });
                        return [2 /*return*/, Promise.reject('Target not found, try the complete name with ! in the end, eg: v1.0.0 users.create!')];
                    case 2:
                        /**
                         * Props Validation
                         */
                        if (type === 'REQ' && endpoint) {
                            v = new validator_1.PropsValidator();
                            params = body.params;
                            schema = endpoint.props;
                            valid = v.validate(schema, params);
                            if (valid !== true) {
                                this.hooks.call('onInvalidProps', { valid: valid, params: params, schema: schema });
                                this.logger.warn("Invalid props, " + valid.message, { label: 'invalid', erros: valid.errors, params: params, schema: schema });
                                return [2 /*return*/, Promise.reject("Invalid props, " + valid.message)];
                            }
                        }
                        stream = endpoint ? this.options.namespace + ":" + catalog_1.Catalog.mountTargetString(endpoint) : target;
                        serializedBody = this.serializer.serialize(body);
                        head = utils_1.mergeObject({
                            format: this.options.serializer.format,
                            sender: this.options.namespace + ":" + this.options.nodeId,
                            receiver: stream
                        }, partialHead);
                        message = new message_1.Message(type, serializedBody, head);
                        return [4 /*yield*/, utils_1.to(this.middlewares.sending.process({ message: message, stream: stream }))];
                    case 3:
                        _a = __read.apply(void 0, [_b.sent(), 2]), error = _a[0], data = _a[1];
                        if (!error) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.hooks.call('sendingErrorOnProcess', { target: target, error: error })];
                    case 4:
                        _b.sent();
                        this.logger.warn("Target " + target + " not found", { label: 'invalid', target: target });
                        return [2 /*return*/, Promise.reject('Error on sending message')];
                    case 5: return [4 /*yield*/, this.transporter.send(data.message, stream)];
                    case 6:
                        messageId = _b.sent();
                        return [4 /*yield*/, this.hooks.call('afterSendMessage', { message: { type: type, body: body, head: head }, messageId: messageId, stream: stream })];
                    case 7:
                        _b.sent();
                        this.logger.debug(message.type + " Message send to " + stream, { label: 'messageSend', msg: message, stream: stream, messageId: messageId });
                        return [2 /*return*/, messageId];
                }
            });
        });
    };
    /**
     * Send request and wait a response
     * @param to
     * @param params
     * @param options
     */
    Broker.prototype.call = function (target, params, options) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, id, timeout;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, utils_1.to(this.publish('REQ', target, { params: params, options: options }))];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 2]), error = _a[0], id = _a[1];
                        timeout = (options === null || options === void 0 ? void 0 : options.timeout) || 5000;
                        if (error) {
                            return [2 /*return*/, new Error(error)];
                        }
                        this.pendingRequests[id] = new utils_1.DeferredPromise();
                        return [2 /*return*/, new Promise(function (resolve, reject) {
                                var hasTimeout = false;
                                var resolved = false;
                                /**
                                 * Set timeout
                                 */
                                setTimeout(function () {
                                    hasTimeout = true;
                                    if (resolved)
                                        return;
                                    _this.hooks.call('callTimeout', { id: id });
                                    _this.logger.warn("Call " + id + " has timeout", { label: 'callTimeout', id: id });
                                    reject('Timeout');
                                }, timeout);
                                _this.pendingRequests[id].then(function (msg) {
                                    if (hasTimeout)
                                        return;
                                    resolve(msg);
                                    resolved = true;
                                    _this.hooks.call('beforeCallResolved', { msg: msg });
                                    _this.logger.info('Call successfully resolved', { label: 'callSuccess', msg: msg });
                                });
                            })];
                }
            });
        });
    };
    /**
     * Request
     */
    Broker.prototype.req = function (type, target, data, options) {
        if (type === void 0) { type = 'REQ'; }
        if (data === void 0) { data = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, id, timeout;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, utils_1.to(this.publish(type, target, data))];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 2]), error = _a[0], id = _a[1];
                        timeout = (options === null || options === void 0 ? void 0 : options.timeout) || 5000;
                        if (error) {
                            return [2 /*return*/, new Error(error)];
                        }
                        this.pendingRequests[id] = new utils_1.DeferredPromise();
                        return [2 /*return*/, new Promise(function (resolve, reject) {
                                var hasTimeout = false;
                                var resolved = false;
                                /**
                                 * Set timeout
                                 */
                                setTimeout(function () {
                                    hasTimeout = true;
                                    if (resolved)
                                        return;
                                    _this.hooks.call('callTimeout', { id: id });
                                    _this.logger.warn("Call " + id + " has timeout", { label: 'callTimeout', id: id });
                                    reject('Timeout');
                                }, timeout);
                                _this.pendingRequests[id].then(function (msg) {
                                    if (hasTimeout)
                                        return;
                                    resolve(msg);
                                    resolved = true;
                                    _this.hooks.call('beforeCallResolved', { msg: msg });
                                    // this.logger.info('Call successfully resolved', { label: 'callSuccess', msg })
                                });
                            })];
                }
            });
        });
    };
    /**
     * Start the broker
     */
    Broker.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, connectTransporter, _loop_1, this_1, _b, _c, _i, key, state_1;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.hooks.call('beforeStart', { broker: this })];
                    case 1:
                        _d.sent();
                        this.logger.info('Starting broker ...', { options: this.options });
                        this.registerCommands();
                        Broker.middlewareResolvePendingRequest(this);
                        Broker.middlewareCommandMessage(this);
                        /**
                         * Listen this broker
                         */
                        return [4 /*yield*/, this.listen({
                                address: { domain: '$all' },
                                local: true,
                                type: 'private',
                                nodes: [this.options.nodeId]
                            }, { group: this.options.nodeId, consumer: this.options.nodeId, initialId: '>' })];
                    case 2:
                        /**
                         * Listen this broker
                         */
                        _d.sent();
                        return [4 /*yield*/, this.listen({
                                address: { domain: this.options.nodeId },
                                local: true,
                                type: 'private',
                                nodes: [this.options.nodeId]
                            }, { group: this.options.nodeId, consumer: this.options.nodeId })
                            /**
                             * Connect transporter
                             */
                        ];
                    case 3:
                        _d.sent();
                        return [4 /*yield*/, utils_1.to(this.hooks.call('connectTransporter', { transporter: this.transporter }, function () { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2 /*return*/, this.transporter.connect()];
                                });
                            }); }))];
                    case 4:
                        _a = __read.apply(void 0, [_d.sent(), 2]), error = _a[0], connectTransporter = _a[1];
                        if (!!connectTransporter) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.hooks.call('impossibleConnectTransporter', { error: error })];
                    case 5:
                        _d.sent();
                        this.logger.error('Error on connect to transporter', { label: 'criticalError', error: error });
                        return [2 /*return*/, new Error('Impossible to conect on transporter')];
                    case 6:
                        _loop_1 = function (key) {
                            var inbox, _a, connectInbox, _b, error_1;
                            return __generator(this, function (_c) {
                                switch (_c.label) {
                                    case 0:
                                        inbox = this_1.inboxes[key];
                                        return [4 /*yield*/, utils_1.to(this_1.hooks.call('connectInbox', { inbox: inbox }, function () { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    return [2 /*return*/, inbox.transporter.connect()];
                                                });
                                            }); }))];
                                    case 1:
                                        _a = __read.apply(void 0, [_c.sent(), 2]), connectInbox = _a[1];
                                        if (!connectInbox) {
                                            return [2 /*return*/, { value: new Error('Impossible to conect on inbox') }];
                                        }
                                        return [4 /*yield*/, utils_1.to(inbox.subscribe(inbox))];
                                    case 2:
                                        _b = __read.apply(void 0, [_c.sent(), 1]), error_1 = _b[0];
                                        if (!error_1) return [3 /*break*/, 4];
                                        return [4 /*yield*/, this_1.hooks.call('impossibleCreateInbox', { inbox: inbox, error: error_1 })];
                                    case 3:
                                        _c.sent();
                                        this_1.logger.error('Error create inbox', { label: 'error' });
                                        return [2 /*return*/, { value: new Error('Error on start inbox') }];
                                    case 4: return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        _b = [];
                        for (_c in this.inboxes)
                            _b.push(_c);
                        _i = 0;
                        _d.label = 7;
                    case 7:
                        if (!(_i < _b.length)) return [3 /*break*/, 10];
                        key = _b[_i];
                        return [5 /*yield**/, _loop_1(key)];
                    case 8:
                        state_1 = _d.sent();
                        if (typeof state_1 === "object")
                            return [2 /*return*/, state_1.value];
                        _d.label = 9;
                    case 9:
                        _i++;
                        return [3 /*break*/, 7];
                    case 10: 
                    /**
                     * Send INFO message to all brokers
                     */
                    return [4 /*yield*/, Broker.scoutInfo(this)];
                    case 11:
                        /**
                         * Send INFO message to all brokers
                         */
                        _d.sent();
                        return [4 /*yield*/, this.hooks.call('afterStart', { broker: this })];
                    case 12:
                        _d.sent();
                        this.logger.info('Broker started', { label: 'started', options: this.options });
                        // Wait info response
                        return [4 /*yield*/, utils_1.delay(1000)];
                    case 13:
                        // Wait info response
                        _d.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // ==========================================================================
    // Extends and functionalities
    // ==========================================================================
    /**
     * Register an endpoint array or a service into broker
     */
    Broker.prototype.register = function (endpointOrService) {
        return __awaiter(this, void 0, void 0, function () {
            var endpoints;
            var _this = this;
            return __generator(this, function (_a) {
                if (!endpointOrService) {
                    return [2 /*return*/, false];
                }
                /**
                 * If is a service interface
                 */
                if (endpointOrService.service) {
                    // Register one endpoint for action
                    endpointOrService.actions.forEach(function (action) { return __awaiter(_this, void 0, void 0, function () {
                        var _a, error;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0: return [4 /*yield*/, utils_1.to(this.listen(action.endpoint, {}, action.resource))];
                                case 1:
                                    _a = __read.apply(void 0, [_b.sent(), 1]), error = _a[0];
                                    if (error) {
                                        this.hooks.call('errorOnRegisterService', { action: action, error: error });
                                        this.logger.error("Error on register service " + action.endpoint.address.domain + "." + action.endpoint.address.path + ", " + (error.message || ''), { label: 'error', action: action, error: error });
                                    }
                                    else {
                                        this.hooks.call('onRegisterService', { action: action });
                                    }
                                    return [2 /*return*/];
                            }
                        });
                    }); });
                    return [2 /*return*/, true];
                }
                endpoints = Array.isArray(endpointOrService) ? endpointOrService : [endpointOrService];
                endpoints.forEach(function (endpoint) {
                    if (!endpoint.address) {
                        return;
                    }
                    _this.hooks.call('beforeRegisterEndpoint', { endpoint: endpoint });
                    _this.catalog.add(endpoint);
                    _this.logger.debug("Register endpoint " + endpoint.address.domain + " total: " + _this.catalog.length);
                    _this.hooks.call('afterRegisterEndpoint', { endpoint: endpoint });
                });
                return [2 /*return*/];
            });
        });
    };
    /**
     * Add middleware function
     */
    Broker.prototype.use = function (middlewares) {
        for (var middlewareName in middlewares) {
            var middleware = middlewares[middlewareName];
            if (this.middlewares[middlewareName]) {
                this.middlewares[middlewareName].use(middleware);
            }
        }
    };
    /**
     * Extend broker and allow build plugins
     */
    Broker.prototype.extend = function (plugin) {
        plugin(this);
    };
    // ==========================================================================
    // Abstract functions
    // ==========================================================================
    /**
     * Send initial info message to all brokers and register external endpoints
     * @param broker
     */
    Broker.scoutInfo = function (broker) {
        return __awaiter(this, void 0, void 0, function () {
            var endpoints, _a, error;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        endpoints = JSON.parse(JSON.stringify(broker.catalog.getAll));
                        // Get only endpoints public and local
                        endpoints = endpoints.filter(function (ep) { return ep.local && ep.type === 'public'; });
                        return [4 /*yield*/, utils_1.to(broker.publish('INFO', '$all', { endpoints: endpoints }))];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 1]), error = _a[0];
                        if (error) {
                            broker.hooks.call('infoImpossibleSendMessage', { error: error, endpoints: endpoints });
                            broker.logger.warn('Could not send info message', { label: 'invalid', error: error, endpoints: endpoints });
                        }
                        /**
                         * Register middleware that will interpret INFO messages and respond with own information
                         */
                        broker.use({
                            receiving: function (_a, next) {
                                var message = _a.message;
                                return __awaiter(_this, void 0, void 0, function () {
                                    var type, head, endpoints_1;
                                    return __generator(this, function (_b) {
                                        switch (_b.label) {
                                            case 0:
                                                type = message.type, head = message.head;
                                                if (type !== 'INFO') {
                                                    return [2 /*return*/, next()];
                                                }
                                                broker.hooks.call('onMessageInfoReceive', { message: message });
                                                if (!message.body.endpoints) return [3 /*break*/, 2];
                                                endpoints_1 = message.body.endpoints.map(function (ep) { ep.local = false; return ep; })
                                                    .map(function (ep) { if (!ep.nodes) {
                                                    ep.nodes = [message.head.sender];
                                                } return ep; });
                                                return [4 /*yield*/, broker.register(endpoints_1)];
                                            case 1:
                                                _b.sent();
                                                _b.label = 2;
                                            case 2:
                                                if (!!head.answering) return [3 /*break*/, 4];
                                                return [4 /*yield*/, broker.publish('INFO', head.sender + "!", { endpoints: endpoints }, { answering: head.id })];
                                            case 3:
                                                _b.sent();
                                                _b.label = 4;
                                            case 4:
                                                next();
                                                return [2 /*return*/];
                                        }
                                    });
                                });
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Register commands
     * @param broker
     */
    Broker.prototype.registerCommands = function () {
        var _this = this;
        this.hooks.register('catalog.removeByNode', function (_a, next) {
            var node = _a.node;
            if (node) {
                _this.catalog.removeByNode(node);
            }
            next();
        });
    };
    // ==========================================================================
    // Middlewares functions
    // ==========================================================================
    /**
     * Middleare responsable for intercepting response messages and resolving pending requests
     * @param broker
     */
    Broker.middlewareResolvePendingRequest = function (broker) {
        broker.pendingRequests = {};
        broker.use({
            receiving: function (_a, next) {
                var message = _a.message, stream = _a.stream, group = _a.group;
                var type = message.type, head = message.head;
                if (head.answering && broker.pendingRequests[head.answering]) {
                    broker.pendingRequests[head.answering].resolve(message);
                }
                next();
            }
        });
    };
    /**
     * Middleare responsable for intercepting CMD messages and run
     * @param broker
     */
    Broker.middlewareCommandMessage = function (broker) {
        broker.use({
            receiving: function (_a, next) {
                var message = _a.message, stream = _a.stream, group = _a.group;
                var type = message.type, body = message.body;
                if (type !== 'CMD') {
                    return next();
                }
                broker.hooks.call(body.cmd, body.params);
                next();
            }
        });
    };
    return Broker;
}());
exports.Broker = Broker;
