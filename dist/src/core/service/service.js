"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var endpoint_1 = require("../../packages/endpoint");
/**
 * Start a new service
 */
var Service = /** @class */ (function () {
    /**
     * Create an instance of Service
     */
    function Service(object) {
        this.service = object.service;
        this.version = object.version;
        this.actions = [];
    }
    /**
     * Register new action into service
     */
    Service.prototype.action = function (action) {
        var name = action.name, props = action.props, config = action.config, resource = __rest(action, ["name", "props", "config"]);
        var endpoint = new endpoint_1.Endpoint({
            address: { domain: this.service, path: name, version: this.version },
            local: true,
            type: 'public',
            props: props,
            config: config
        });
        this.actions.push({ endpoint: endpoint, resource: resource });
    };
    return Service;
}());
exports.Service = Service;
exports.default = Service;
