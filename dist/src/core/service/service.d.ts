import { Action } from './service.interface';
/**
 * Start a new service
 */
export declare class Service {
    readonly service: string;
    readonly version: string;
    private actions;
    /**
     * Create an instance of Service
     */
    constructor(object: {
        service: Service['service'];
        version: Service['version'];
    } & Partial<Service>);
    /**
     * Register new action into service
     */
    action(action: Action): void;
}
export default Service;
