"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../../utils");
var Message = /** @class */ (function () {
    function Message(type, body, head) {
        this.type = type;
        this.body = body;
        this.head = head;
        this.type = type || 'unknown';
        this.body = body || {};
        this.head = utils_1.shallow(head);
    }
    /**
     * Parse a object into a message
     * @param object
     */
    Message.parse = function (object) {
        var type = object.type, body = object.body, head = __rest(object, ["type", "body"]);
        return new Message(type, body, head);
    };
    return Message;
}());
exports.Message = Message;
