import { IMessageType, IMessageBody, IMessageHead } from './message.interface';
export declare class Message<T extends IMessageType, U extends IMessageBody<T>, V extends IMessageHead> {
    type: T;
    body: U;
    head: V;
    constructor(type: T, body: U, head: V);
    /**
     * Parse a object into a message
     * @param object
     */
    static parse(object: any): Message<any, any, any>;
}
