import { Endpoint } from '../endpoint';
/**
 * Message Type
 */
export declare type IMessageType = 'REQ' | 'RES' | 'INFO' | 'EVENT' | 'CMD';
/**
 * Body Message
 */
export declare type IMessageBody<T> = T extends 'REQ' ? IMessageBodyREQ : T extends 'RES' ? IMessageBodyRES : T extends 'CMD' ? IMessageBodyCMD : T extends 'INFO' ? IMessageBodyINFO : never;
/**
 * Message Head
 */
export interface IMessageHead {
    format: string;
    sender: string;
    receiver: string;
    id?: string;
    correlationId?: string;
    answering?: string;
    accept?: string;
    language?: string;
    expires?: string;
}
export interface IMessage {
    type: IMessageType;
    body: any;
    head: IMessageHead;
}
interface IMessageBodyREQ {
    params?: object;
    options?: object;
}
interface IMessageBodyRES {
    data?: object;
    error?: boolean;
}
interface IMessageBodyCMD {
    cmd: string;
    params?: object;
}
interface IMessageBodyINFO {
    endpoints?: Endpoint[];
}
export {};
