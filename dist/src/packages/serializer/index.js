"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var JSON_1 = __importDefault(require("./adapters/JSON"));
exports.Packages = {
    JSON: JSON_1.default
};
exports.serializer = function (adapter) {
    if (!Object.keys(exports.Packages).includes(adapter)) {
        return new Error('Seralizer package not found');
    }
    return new exports.Packages[adapter]();
};
__export(require("./base"));
