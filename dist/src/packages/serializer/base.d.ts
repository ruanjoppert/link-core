/**
 * Base Serializer
 */
export declare abstract class BaseSerializer {
    format: string;
    abstract serialize(object: any): any;
    abstract deserialize(buffer: any): any;
}
