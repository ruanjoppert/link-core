"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Base Serializer
 */
var BaseSerializer = /** @class */ (function () {
    function BaseSerializer() {
        this.format = '';
    }
    return BaseSerializer;
}());
exports.BaseSerializer = BaseSerializer;
