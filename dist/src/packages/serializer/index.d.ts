import JSON from './adapters/JSON';
export declare const Packages: {
    JSON: typeof JSON;
};
export declare type Packages = typeof Packages;
export declare const serializer: <T extends "JSON">(adapter: T) => T extends "JSON" ? InstanceType<{
    JSON: typeof JSON;
}[T]> : Error;
export * from './base';
