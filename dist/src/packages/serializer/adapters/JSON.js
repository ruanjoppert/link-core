"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = require("../base");
var message_1 = require("../../message");
/**
 * Serializer
 *
 * @class Serializer
 * @param {Packet}
 */
var JSONSerializer = /** @class */ (function (_super) {
    __extends(JSONSerializer, _super);
    function JSONSerializer() {
        var _this = _super.call(this) || this;
        _this.format = 'JSON';
        return _this;
    }
    /**
     * Serializer a JS object to Buffer
     */
    JSONSerializer.prototype.serialize = function (message) {
        if (message instanceof message_1.Message) {
            var seriazedMessage = __assign({}, message);
            seriazedMessage.body = Buffer.from(JSON.stringify(seriazedMessage.body || {}));
            return seriazedMessage;
        }
        return Buffer.from(JSON.stringify(message || {}));
    };
    /**
     * Deserialize Buffer to JS object
     */
    JSONSerializer.prototype.deserialize = function (message) {
        if (message.body) {
            message.body = JSON.parse(message.body);
            return message;
        }
        return JSON.parse(message);
    };
    return JSONSerializer;
}(base_1.BaseSerializer));
exports.default = JSONSerializer;
