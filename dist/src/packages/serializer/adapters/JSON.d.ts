import { BaseSerializer } from '../base';
import { IMessage } from '../../message';
/**
 * Serializer
 *
 * @class Serializer
 * @param {Packet}
 */
declare class JSONSerializer extends BaseSerializer {
    constructor();
    /**
     * Serializer a JS object to Buffer
     */
    serialize<T>(message: T): T extends IMessage ? IMessage : any;
    /**
     * Deserialize Buffer to JS object
     */
    deserialize<T extends IMessage>(message: T): T;
}
export default JSONSerializer;
