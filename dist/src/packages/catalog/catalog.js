"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../../utils");
var Catalog = /** @class */ (function () {
    function Catalog() {
        this.endpoints = [];
    }
    /**
     * Add a new endpoint
     * @param endpoint
     */
    Catalog.prototype.add = function (endpoint) {
        if (!endpoint)
            return new Error('Missing params');
        var exists = this.find(endpoint);
        if (exists) {
            var nodes = exists.nodes || [];
            this.remove(exists);
            if (!endpoint.nodes) {
                endpoint.nodes = [];
            }
            endpoint.nodes = nodes.indexOf(endpoint.nodes[0]) <= -1 ? __spread(nodes, endpoint.nodes) : nodes;
        }
        this.endpoints.push(endpoint);
        return endpoint;
    };
    /**
     * Remove a new endpoint
     * @param endpoint
     */
    Catalog.prototype.remove = function (endpoint) {
        var index = -1;
        if (typeof endpoint === 'string') {
            var address_1 = Catalog.parseTargetString(endpoint);
            if (!address_1.domain || !address_1.path || !address_1.version) {
                return new Error('Must be a complete target string to remove from catalog');
            }
            var find = this.endpoints.find(function (ep) {
                if (address_1.domain === ep.address.domain &&
                    address_1.path === ep.address.path &&
                    address_1.version === ep.address.version) {
                    return true;
                }
            });
            if (find) {
                index = this.endpoints.indexOf(find);
            }
            return false;
        }
        index = this.endpoints.indexOf(endpoint);
        if (index > -1) {
            this.endpoints.splice(index, 1);
            return true;
        }
        return false;
    };
    /**
     *
     * @param targetString
     */
    Catalog.prototype.removeByNode = function (nodeId) {
        var _this = this;
        this.endpoints.filter(function (ep) { return ep.nodes.indexOf(nodeId) > -1; })
            .forEach(function (ep) { return ep.nodes.length > 1
            ? _this.endpoints[_this.endpoints.indexOf(ep)].nodes.splice(ep.nodes.indexOf(nodeId), 1)
            : _this.endpoints.splice(_this.endpoints.indexOf(ep), 1); });
    };
    /**
     * Find a endpoint
     * @param targetString
     */
    Catalog.prototype.find = function (targetString) {
        var address = typeof targetString === 'string' ? Catalog.parseTargetString(targetString, false) : targetString.address;
        var search = this.endpoints.filter(function (ep) {
            var find = ep.address.domain === address.domain && ep.address.path === address.path;
            // not found
            if (!find) {
                return false;
            }
            // find and version is not provided
            if (find && !address.version) {
                return true;
            }
            // Find and version is provided
            if (find && address.version && ep.address.version) {
                return utils_1.compareVersion(utils_1.parseVersion(address.version), utils_1.parseVersion(ep.address.version));
            }
        })
            .sort(function (a, b) { return (a.address.version && b.address.version) ? (a.address.version < b.address.version) ? -1 : 1 : 1; })
            .pop();
        if (!search) {
            return false;
        }
        return search;
    };
    Object.defineProperty(Catalog.prototype, "length", {
        /**
         * Return endpoint lengths
         */
        get: function () {
            return this.endpoints.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Catalog.prototype, "getAll", {
        /**
         * Return endpoint lengths
         */
        get: function () {
            return this.endpoints;
        },
        enumerable: true,
        configurable: true
    });
    // ==========================================================================
    /**
     * Parse a target string into adress of Endpoint
     * @param targetString
     * @param fullVersion
     */
    Catalog.parseTargetString = function (targetString, fullVersion) {
        if (fullVersion === void 0) { fullVersion = true; }
        var version;
        var _a = __read(targetString.split(':').reverse(), 2), rest = _a[0], namespace = _a[1];
        var _b = __read(rest.split(' ').reverse(), 2), resource = _b[0], v = _b[1];
        var _c = __read(resource.split('.'), 2), domain = _c[0], path = _c[1];
        if (v) {
            version = v.charAt(0) === 'v' ? v.substring(1) : v;
            if (fullVersion) {
                version = utils_1.parseVersion(version);
                version = Object.keys(version).reduce(function (prev, key) {
                    var value = version[key] | 0;
                    prev.push(value);
                    return prev;
                }, []).join('.');
            }
        }
        return {
            domain: domain,
            path: path,
            version: version
        };
    };
    /**
     * Mount a target string with Endpoint
     * @param endpoint
     */
    Catalog.mountTargetString = function (endpoint) {
        var _a, _b, _c;
        var version = ((_a = endpoint === null || endpoint === void 0 ? void 0 : endpoint.address) === null || _a === void 0 ? void 0 : _a.version) ? "v" + endpoint.address.version + " " : '';
        var domain = (_b = endpoint === null || endpoint === void 0 ? void 0 : endpoint.address) === null || _b === void 0 ? void 0 : _b.domain;
        var path = ((_c = endpoint === null || endpoint === void 0 ? void 0 : endpoint.address) === null || _c === void 0 ? void 0 : _c.path) ? "." + endpoint.address.path : '';
        return "" + version + domain + path;
    };
    return Catalog;
}());
exports.Catalog = Catalog;
