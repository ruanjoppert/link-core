import { Endpoint, IEndpointAdress } from '../endpoint';
export declare class Catalog {
    private endpoints;
    /**
     * Add a new endpoint
     * @param endpoint
     */
    add(endpoint: Endpoint): Error | Endpoint;
    /**
     * Remove a new endpoint
     * @param endpoint
     */
    remove(endpoint: Endpoint | string): boolean | Error;
    /**
     *
     * @param targetString
     */
    removeByNode(nodeId: string): void;
    /**
     * Find a endpoint
     * @param targetString
     */
    find(targetString: Endpoint | string): false | Endpoint;
    /**
     * Return endpoint lengths
     */
    get length(): number;
    /**
     * Return endpoint lengths
     */
    get getAll(): Endpoint[];
    /**
     * Parse a target string into adress of Endpoint
     * @param targetString
     * @param fullVersion
     */
    static parseTargetString(targetString: string, fullVersion?: boolean): IEndpointAdress;
    /**
     * Mount a target string with Endpoint
     * @param endpoint
     */
    static mountTargetString(endpoint: Endpoint): string;
}
