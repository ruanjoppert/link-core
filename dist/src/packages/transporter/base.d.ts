import { IMessage } from '../message';
/**
 * Base Transporter
 */
export declare abstract class BaseTransporter {
    options: any;
    isConnected: boolean;
    client: any;
    /**
     * Create an instance of BaseTransporter
     */
    constructor(options: any);
    abstract connect(): Promise<boolean | Error>;
    abstract disconnect(): void;
    abstract send(data: IMessage, channel: string): Promise<string>;
    abstract read(channel: string, consumer: string, group: string): Promise<IMessage | Error>;
    connected(): void;
    disconnected(): void;
    /**
     * Event hander for connected
     */
    onConnected(client: object): void | Promise<void>;
    /**
     * Event hander for disconnected
     */
    onDisconnected(): void | Promise<void>;
    /**
     * Event handler for error
     */
    onError(error: string): Error;
}
