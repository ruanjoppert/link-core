import Redis from './adapters/redis';
export declare const Packages: {
    Redis: typeof Redis;
};
export declare type Packages = typeof Packages;
export declare const transporter: <T extends "Redis">(adapter: T, options?: object) => T extends "Redis" ? InstanceType<{
    Redis: typeof Redis;
}[T]> : Error;
export * from './base';
