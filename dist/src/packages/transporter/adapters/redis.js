"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = require("../base");
var message_1 = require("../../message");
var utils_1 = require("../../../utils");
/**
 * Redis Transporter
 */
var RedisTransporter = /** @class */ (function (_super) {
    __extends(RedisTransporter, _super);
    /**
     * RedisTransporter
     * @param {*} options
     */
    function RedisTransporter(options) {
        var _this = _super.call(this, __assign(__assign({}, options), { showFriendlyErrorStack: true })) || this;
        _this.lastId = _this.options.initialId || '0-0';
        return _this;
    }
    /**
     * Connect to server
     */
    RedisTransporter.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, IOredis, client, isConnected, _b, connectionError;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (this.isConnected)
                            return [2 /*return*/, true];
                        _a = __read(utils_1.to(require)('ioredis'), 2), error = _a[0], IOredis = _a[1];
                        if (error) {
                            return [2 /*return*/, Promise.reject(new Error('Package ioredis not found, please try install with yarn add ioredis and try again.'))];
                        }
                        client = new IOredis(this.options);
                        isConnected = new utils_1.DeferredPromise();
                        client.on('connect', function () {
                            isConnected.resolve();
                            _this.onConnected(client);
                        });
                        client.on('close', function () { return _this.onDisconnected(); });
                        client.on('error', function (err) {
                            isConnected.reject(err);
                            _this.onError(err);
                        });
                        return [4 /*yield*/, utils_1.to(isConnected)];
                    case 1:
                        _b = __read.apply(void 0, [_c.sent(), 1]), connectionError = _b[0];
                        if (connectionError) {
                            return [2 /*return*/, Promise.reject(new Error(connectionError))];
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     * Disconnect from server
     */
    RedisTransporter.prototype.disconnect = function () {
        if (this.client) {
            this.client.disconnect();
        }
    };
    /**
     * Publish a packet into a channel
     */
    RedisTransporter.prototype.send = function (message, stream) {
        return __awaiter(this, void 0, void 0, function () {
            var type, body, head, messageToBeSend, arrMessage, _a, error, messageId;
            var _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!message || !stream) {
                            return [2 /*return*/, new Error('Missing params')];
                        }
                        type = message.type, body = message.body, head = message.head;
                        messageToBeSend = __assign({ type: type, body: body }, head);
                        arrMessage = Object.keys(messageToBeSend).reduce(function (prev, key) {
                            var value = messageToBeSend[key];
                            if (!value || utils_1.isObject(value))
                                return prev;
                            prev.push(key, value);
                            return prev;
                        }, []);
                        if (arrMessage.indexOf('body') <= -1) {
                            return [2 /*return*/, new Error('The body is not present, it may not have been serialized correctly')];
                        }
                        return [4 /*yield*/, utils_1.to((_b = this.client).xadd.apply(_b, __spread([stream, '*'], arrMessage)))];
                    case 1:
                        _a = __read.apply(void 0, [_c.sent(), 2]), error = _a[0], messageId = _a[1];
                        if (error) {
                            return [2 /*return*/, new Error(error)];
                        }
                        return [2 /*return*/, messageId];
                }
            });
        });
    };
    /**
     * Subscribe to a channel with group and consumer
     * @param {String} group
     * @param {String} consumer
     * @param {String} stream
     * @returns void
     */
    RedisTransporter.prototype.read = function (stream, consumer, group) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, reply, results, _b, _c, id, message, objectMessage;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, utils_1.to(this.client.xreadgroup('GROUP', group, consumer, 'COUNT', 1, 'BLOCK', '5000', 'STREAMS', stream, this.lastId))
                        /**
                         * If have error, throw
                         */
                    ];
                    case 1:
                        _a = __read.apply(void 0, [_d.sent()
                            /**
                             * If have error, throw
                             */
                            , 2]), error = _a[0], reply = _a[1];
                        /**
                         * If have error, throw
                         */
                        if (error) {
                            console.log('error xreadgroup', error);
                            return [2 /*return*/, Promise.reject(error)];
                        }
                        if (!reply) {
                            return [2 /*return*/, this.read(stream, consumer, group)];
                        }
                        results = reply[0][1];
                        if (!results.length) {
                            this.lastId = '>';
                            return [2 /*return*/, this.read(stream, consumer, group)];
                        }
                        _b = __read(results, 1), _c = __read(_b[0], 2), id = _c[0], message = _c[1];
                        this.lastId = id;
                        objectMessage = utils_1.chunkArray(message, 2).reduce(function (obj, _a) {
                            var _b = __read(_a, 2), key = _b[0], value = _b[1];
                            obj[key] = value;
                            return obj;
                        }, { id: id });
                        // console.log('chegou uma mensagem aqui', objectMessage)
                        return [2 /*return*/, message_1.Message.parse(objectMessage)];
                }
            });
        });
    };
    /**
     * Mark a message as ack
     * @param stream
     * @param group
     * @param messageId
     */
    RedisTransporter.prototype.ack = function (stream, group, messageId) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, poa;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, utils_1.to(this.client.xack(stream, group, messageId))];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 2]), error = _a[0], poa = _a[1];
                        if (error) {
                            return [2 /*return*/, false];
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     * Check if exists channel and/or group in channel
     * @param {*} channel
     * @param {*} group
     */
    RedisTransporter.prototype.exists = function (stream, group) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error, groups, inGroup;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, utils_1.to(this.client.xinfo('GROUPS', stream))];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 2]), error = _a[0], groups = _a[1];
                        if (!group) {
                            return [2 /*return*/, groups !== null];
                        }
                        inGroup = (error && error.message) === 'ERR no such key'
                            ? false
                            : groups.map(function (arr) { return utils_1.fromEntries(utils_1.chunkArray(arr, 2)).name === group; })
                                .filter(Boolean).length;
                        return [2 /*return*/, inGroup > 0];
                }
            });
        });
    };
    /**
     * Create one stream, group and enter on it
     * @param {String} channel
     * @param {String} group
     */
    RedisTransporter.prototype.createStream = function (stream, group) {
        return __awaiter(this, void 0, void 0, function () {
            var exists, _a, error;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.exists(stream, group)];
                    case 1:
                        exists = _b.sent();
                        if (exists) {
                            return [2 /*return*/, true];
                        }
                        return [4 /*yield*/, utils_1.to(this.client.xgroup('CREATE', stream, group, '$', 'MKSTREAM'))];
                    case 2:
                        _a = __read.apply(void 0, [_b.sent(), 1]), error = _a[0];
                        if (error) {
                            return [2 /*return*/, new Error(error)];
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return RedisTransporter;
}(base_1.BaseTransporter));
exports.default = RedisTransporter;
