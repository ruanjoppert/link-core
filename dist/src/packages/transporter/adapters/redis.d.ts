import { BaseTransporter } from '../base';
import { IMessage } from '../../message';
/**
 * Redis Transporter
 */
declare class RedisTransporter extends BaseTransporter {
    client: any;
    private lastId;
    /**
     * RedisTransporter
     * @param {*} options
     */
    constructor(options: any);
    /**
     * Connect to server
     */
    connect(): Promise<boolean>;
    /**
     * Disconnect from server
     */
    disconnect(): void;
    /**
     * Publish a packet into a channel
     */
    send(message: IMessage, stream: string): Promise<any>;
    /**
     * Subscribe to a channel with group and consumer
     * @param {String} group
     * @param {String} consumer
     * @param {String} stream
     * @returns void
     */
    read(stream: string, consumer: string, group: string): Promise<IMessage | Error>;
    /**
     * Mark a message as ack
     * @param stream
     * @param group
     * @param messageId
     */
    ack(stream: string, group: string, messageId: string): Promise<boolean>;
    /**
     * Check if exists channel and/or group in channel
     * @param {*} channel
     * @param {*} group
     */
    exists(stream: string, group?: string): Promise<boolean>;
    /**
     * Create one stream, group and enter on it
     * @param {String} channel
     * @param {String} group
     */
    createStream(stream: string, group: string): Promise<boolean | Error>;
}
export default RedisTransporter;
