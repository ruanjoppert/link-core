"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var redis_1 = __importDefault(require("./adapters/redis"));
exports.Packages = {
    Redis: redis_1.default
};
exports.transporter = function (adapter, options) {
    if (options === void 0) { options = {}; }
    if (!Object.keys(exports.Packages).includes(adapter)) {
        return new Error('Seralizer package not found');
    }
    return new exports.Packages[adapter](options);
};
__export(require("./base"));
