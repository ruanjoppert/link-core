"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Base Transporter
 */
var BaseTransporter = /** @class */ (function () {
    /**
     * Create an instance of BaseTransporter
     */
    function BaseTransporter(options) {
        this.options = options;
        this.isConnected = false;
        this.client = null;
    }
    BaseTransporter.prototype.connected = function () { };
    BaseTransporter.prototype.disconnected = function () { };
    /**
     * Event hander for connected
     */
    BaseTransporter.prototype.onConnected = function (client) {
        this.isConnected = true;
        this.client = client;
        if (this.connected) {
            return this.connected();
        }
        return Promise.resolve();
    };
    /**
     * Event hander for disconnected
     */
    BaseTransporter.prototype.onDisconnected = function () {
        this.client = null;
        this.isConnected = false;
        if (this.disconnected) {
            return this.disconnected();
        }
        return Promise.resolve();
    };
    /**
     * Event handler for error
     */
    BaseTransporter.prototype.onError = function (error) {
        return new Error(error);
    };
    return BaseTransporter;
}());
exports.BaseTransporter = BaseTransporter;
