export declare class Pipeline {
    middlewares: any[];
    data: any;
    finished: boolean;
    constructor(middlewares: any, data: any);
    /**
     * @data stays in a closure here
     */
    dispatch(): Promise<any>;
}
