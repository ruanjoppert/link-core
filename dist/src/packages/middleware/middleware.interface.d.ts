export declare type nextType = (data?: any) => void;
export declare type endType = (data?: any, isError?: boolean) => void;
