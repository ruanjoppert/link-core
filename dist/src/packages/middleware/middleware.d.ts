import { nextType, endType } from './middleware.interface';
export declare class Middleware {
    private middlewares;
    use(middleware: (data: any, next: nextType, end: endType) => any): void;
    process(data: any): Promise<any>;
}
