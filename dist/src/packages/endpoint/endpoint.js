"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Endpoint
 */
var Endpoint = /** @class */ (function () {
    function Endpoint(endpoint) {
        this.address = endpoint === null || endpoint === void 0 ? void 0 : endpoint.address;
        this.local = (endpoint === null || endpoint === void 0 ? void 0 : endpoint.local) || false;
        this.type = (endpoint === null || endpoint === void 0 ? void 0 : endpoint.type) || 'public';
        this.props = (endpoint === null || endpoint === void 0 ? void 0 : endpoint.props) || {};
        this.config = (endpoint === null || endpoint === void 0 ? void 0 : endpoint.config) || {};
        this.nodes = (endpoint === null || endpoint === void 0 ? void 0 : endpoint.nodes) || [];
    }
    return Endpoint;
}());
exports.Endpoint = Endpoint;
