export declare type IEndpointAdress = {
    domain: string;
    path?: string;
    version?: string;
};
