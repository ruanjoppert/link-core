import { IEndpointAdress } from './endpoint.interface';
/**
 * Endpoint
 */
export declare class Endpoint {
    address: IEndpointAdress;
    local: boolean;
    type?: 'public' | 'private';
    props?: any;
    config?: any;
    nodes: string[];
    constructor(endpoint: {
        address: IEndpointAdress;
        local?: boolean;
        type?: 'public' | 'private';
        props?: any;
        config?: any;
        nodes?: any;
    });
}
