import { IMessage } from '../message';
import { Broker } from '../../core/broker';
export declare class Reply {
    private broker;
    private message;
    private ack;
    [index: string]: any;
    constructor(broker: Broker, message: IMessage, ack: Function);
    /**
     * Send a RES message for the sender
     * @param response
     */
    send(response: any, finish?: boolean): Promise<void>;
    /**
     * Send a RES message for the sender with error
     * @param response
     */
    error(response: any, finish?: boolean): Promise<void>;
    /**
     * Finish without send anything
     */
    end(finish?: boolean): void;
}
