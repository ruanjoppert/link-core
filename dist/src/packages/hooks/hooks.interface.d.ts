export interface IHook {
    name: string;
    [index: string]: ((data: any, next: any, end: any) => any) | string;
}
