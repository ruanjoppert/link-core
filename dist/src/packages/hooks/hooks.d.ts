export declare class Hooks {
    private hooks;
    /**
     * Register hook
     */
    register(hookName: string, hook: (data: {
        [index: string]: any;
    }, next: any) => any): boolean;
    /**
     * Call hook
     * @param hookName
     * @param data
     * @param handler
     */
    call(hookName: string, data?: any, handler?: any): Promise<any>;
}
