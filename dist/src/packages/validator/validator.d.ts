export declare class PropsValidator {
    private validator;
    constructor();
    compile(schema: any): any;
    validate(schema: any, params: any): true | {
        message: any;
        errors: any;
    };
}
