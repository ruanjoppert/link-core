"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ajv_1 = __importDefault(require("ajv"));
var PropsValidator = /** @class */ (function () {
    function PropsValidator() {
        this.validator = new ajv_1.default({ removeAdditional: false });
    }
    PropsValidator.prototype.compile = function (schema) {
        try {
            var compile = this.validator.compile(schema);
            return true;
        }
        catch (error) {
            return error;
        }
    };
    PropsValidator.prototype.validate = function (schema, params) {
        var valid = this.validator.validate(schema, params);
        if (!valid) {
            return {
                message: this.validator.errorsText(),
                errors: this.validator.errors
            };
        }
        return true;
    };
    return PropsValidator;
}());
exports.PropsValidator = PropsValidator;
