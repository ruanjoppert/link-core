# Ligue Messaging Broker

- [Instalação](#instalação)
- [Como usar](#como-usar)
- [Plugins](#plugins)

## Instalação

Como o projeto é particular é necessário usar uma chave de acesso e um comando maior para instalar

```sh
npm install git+https://ruanjoppert:Nzheer7CVcdggVTanrcJ@bitbucket.org:ligue/link.git
```

## Criando o primeiro serviço

O exemplo abaixo mostra como é possivel criar um serviço conectando em um stream server de testes

```ts
import { Broker } from 'link'

// Criamos o broker
const app = new Broker({
  namespace: 'development',

  transporter: {
    adapter: 'Redis',

    options: {
      host: '177.125.208.14',
      port: 6333
    }
  }
})

// Adicionamos um novo serviço
const Math = new Service({
  service: 'math',
  version: '1.0.0'
})

// Adicionamos uma ação
Math.action({
  name: 'sum',

  handler (message, reply) {
    const { a, b } = message.body.params

    reply.send({ result: a + b })
  }
})

// Startamos o serviço
app.start()
```

Após isso é possivel em qualquer serviço conecto ao mesmo servidor de stream e em um mesmo **namespace** realizar chamadas para este serviço usando o método call ```app.call('math.sum', { a: 5, b: 1 })```

## Plugins

  - TaskQueue: Agenda requisições para serem executadas em um intervalo de tempo futuro com um número de tentativas determinada
