import { Hooks } from '../../../src/packages/hooks'

describe('Hooks', () => {
  it('should return false if params is not provided', () => {
    const hook = new Hooks()

    expect(hook.register('name')).toBeFalsy()
  })

  it('should add if params is provided', () => {
    const hook = new Hooks()
    const register = hook.register('name', () => {

    })

    expect(register).toBeTruthy()
  })

  it('should call hook', async () => {
    const hook = new Hooks()
    const user = { user: 'jhon' }

    const register = hook.register('addLastname', (data, next) => {
      data.lastname = 'doe'

      next()
    })

    const data = await hook.call('addLastname', user)

    expect(register).toBeTruthy()
    expect(data).toHaveProperty('lastname')
  })

  it('should call default hook', async () => {
    const hook = new Hooks()
    const user: any = { user: 'jhon' }

    const withEmail = await hook.call('addEmail', user, async (data: any) => {
      data.email = 'email@email.com'

      return data
    })

    expect(withEmail).toHaveProperty('email')
  })
})
