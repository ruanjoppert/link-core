import { Message } from '../../../src/packages/message'

describe('Message', () => {
  it('should be return a "unknown" message type if type is not provided', () => {
    const message = new Message('')

    expect(message.type).toBe('unknown')
    expect(message.body).toEqual({})
    expect(message.head).toBeUndefined()
  })

  it('should be return a empty object if body is not provided', () => {
    const message = new Message('')

    expect(message.type).toBe('unknown')
    expect(message.body).toEqual({})
    expect(message.head).toBeUndefined()
  })

  it('should be return a undefined head if head is not provided', () => {
    const message = new Message('')

    expect(message.type).toBe('unknown')
    expect(message.body).toEqual({})
    expect(message.head).toBeUndefined()
  })

  it('should be return type and data if type is provided', () => {
    const body = { params: { name: 'jhon' } }
    const message = new Message('REQ', body)

    expect(message.type).toBe('REQ')
    expect(message.body).toEqual(body)
    expect(message.head).toBeUndefined()
  })

  it('should be shallow the head if is provided', () => {
    const body = { params: { name: 'jhon' } }
    const head = { accept: { format: 'json', language: 'pt-br' }, sender: 'sender-0001' }

    const message = new Message('REQ', body, head)

    expect(message.type).toBe('REQ')
    expect(message.body).toEqual(body)
    expect(message.head).toEqual({ acceptFormat: 'json', acceptLanguage: 'pt-br', sender: 'sender-0001' })
  })
})
