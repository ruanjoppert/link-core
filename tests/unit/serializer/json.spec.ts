import { serializer } from '../../../src/packages/serializer'
import { Message, IMessage } from '../../../src/packages/message'

describe('JSON Serializer', () => {
  const Serializer = serializer('JSON')

  const body = { user: { name: 'Jhon', email: 'jhon@email.com', address: { zipcode: '87000-000' } } }
  const head = { format: 'JSON', sender: 'sender', receiver: 'receiver' }

  const message = new Message('REQ', { params: body }, head)

  let serializedBody: any
  let serializedMessage: IMessage

  it('should be a json serialized', () => {
    expect(Serializer.format).toBe('JSON')
    expect(Serializer.serialize).toBeDefined()
    expect(Serializer.deserialize).toBeDefined()
  })

  it('should be be serialize a object', () => {
    serializedBody = Serializer.serialize(body)

    expect(serializedBody).toBeInstanceOf(Buffer)
  })

  it('should be deserialize a object', () => {
    const deserializedBody = Serializer.deserialize(serializedBody)

    expect(deserializedBody).toEqual(body)
  })

  it('should be serialize a message', () => {
    serializedMessage = Serializer.serialize(message)

    expect(serializedMessage.type).toBe('REQ')
    expect(serializedMessage.body).not.toEqual(body)
    expect(serializedMessage.head).toEqual(head)
  })

  it('should be deserialize a message', () => {
    const deserializedMessage = Serializer.deserialize(
      Serializer.serialize(message)
    )

    expect(message).toEqual(deserializedMessage)
    expect(message.body).toEqual({ params: body })
    expect(message.head).toEqual(head)
  })
})
