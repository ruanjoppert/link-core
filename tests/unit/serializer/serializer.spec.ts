import { serializer, BaseSerializer } from '../../../src/packages/serializer'

describe('Serializer', () => {
  it('should be return an error if adapter is not provided', () => {
    const Serializer = serializer()

    expect(Serializer).toBeInstanceOf(Error)
    expect(Serializer).toEqual(new Error('Seralizer package not found'))
  })

  it('should be return an error if adapter provided is invalid', () => {
    const Serializer = serializer('nonValidAdapter')

    expect(Serializer).toBeInstanceOf(Error)
    expect(Serializer).toEqual(new Error('Seralizer package not found'))
  })

  it('should be return a valid serializer if adapter is provided', () => {
    const Serializer = serializer('JSON')

    expect(Serializer).toBeInstanceOf(BaseSerializer)
  })
})
