import { Endpoint } from '../../../src/packages/endpoint'

describe('Endpoint', () => {
  it('should be create a endpoint with default values', () => {
    const endpoint = new Endpoint()

    console.log(endpoint)

    expect(endpoint.address).toBeUndefined()
    expect(endpoint.local).toBe(false)
    expect(endpoint.type).toBe('public')
    expect(endpoint.props).toEqual({})
    expect(endpoint.config).toEqual({})
  })

  it('should be create a endpoint with address provided', () => {
    const address = { domain: 'users', path: 'findOne', version: '1.0.0' }
    const endpoint = new Endpoint({ address, local: true, type: 'private' })

    expect(endpoint.address).toEqual(address)
    expect(endpoint.local).toBe(true)
    expect(endpoint.type).toBe('private')
    expect(endpoint.props).toEqual({})
    expect(endpoint.config).toEqual({})
  })
})
