import { transporter, BaseTransporter } from '../../../src/packages/transporter'

describe('Transporter adapter', () => {
  it('should be return an error if adapter is not provided', () => {
    const Transporter = transporter()

    expect(Transporter).toBeInstanceOf(Error)
    expect(Transporter).toEqual(new Error('Seralizer package not found'))
  })

  // it('should be return an error if adapter provided is invalid', () => {
  //   const Transporter = transporter('nonValidAdapter')

  //   expect(Transporter).toBeInstanceOf(Error)
  //   expect(Transporter).toEqual(new Error('Seralizer package not found'))
  // })

  // it('should be return a valid transporter if adapter is provided', () => {
  //   const Transporter = transporter('Redis')

  //   expect(Transporter).toBeInstanceOf(BaseTransporter)
  // })

  // it('should be set options if is provided', () => {
  //   const options = { port: 123456 }
  //   const Transporter = transporter('Redis', options)

  //   expect(Transporter).toBeInstanceOf(BaseTransporter)
  //   expect(Transporter.options).toMatchObject(options)
  // })
})
