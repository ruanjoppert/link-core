import { Middleware } from '../../../src/packages/middleware/middleware'

describe('Middleware Manager', () => {
  it('should register middlewares in order', async () => {
    const middleware = new Middleware()
    const data = { name: 'Ruan' }

    middleware.use((data, next) => {
      data.lastname = 'Joppert'
      next()
    })

    middleware.use((data, next) => {
      data.email = 'ruan.joppert@gmail.com'
      next()
    })

    const user = await middleware.process(data)

    expect(user.name).toBe('Ruan')
    expect(user.lastname).toBe('Joppert')
    expect(user.email).toBe('ruan.joppert@gmail.com')
  })

  it('should be possible overwrite data calling next with data', async () => {
    const middleware = new Middleware()
    const data = { name: 'Ruan' }

    middleware.use((data, next) => {
      data.lastname = 'Joppert'
      next()
    })

    middleware.use((data, next) => {
      data.email = 'ruan.joppert@gmail.com'
      next('overwrite')
    })

    const user = await middleware.process(data)

    expect(user).toBe('overwrite')
  })

  it('should be possible end the middleware early with end()', async () => {
    const middleware = new Middleware()
    const data = { name: 'Ruan' }

    middleware.use((data, next, end) => {
      data.lastname = 'Joppert'

      end()
    })

    middleware.use((data, next) => {
      data.email = 'ruan.joppert@gmail.com'
      next()
    })

    const user = await middleware.process(data)

    expect(user.name).toBe('Ruan')
    expect(user.lastname).toBe('Joppert')
    expect(user.email).toBeUndefined()
  })

  it('should be possible end the middleware early overwrite data with end()', async () => {
    const middleware = new Middleware()
    const data = { name: 'Ruan' }

    middleware.use((data, next, end) => {
      data.lastname = 'Joppert'

      end('overwrite')
    })

    middleware.use((data, next) => {
      data.email = 'ruan.joppert@gmail.com'
      next()
    })

    const user = await middleware.process(data)

    expect(user).toBe('overwrite')
  })

  it('should be possible end as error with end()', async () => {
    const middleware = new Middleware()
    const data = { name: 'Ruan' }

    middleware.use((data, next, end) => {
      data.lastname = 'Joppert'

      end({ error: 'has an error here' }, true)
    })

    middleware.use((data, next) => {
      data.email = 'ruan.joppert@gmail.com'
      next()
    })

    const user = middleware.process(data)

    expect(user).rejects.toBeTruthy()
    await expect(user).rejects.toMatchObject({ data: { name: 'Ruan', lastname: 'Joppert' }, error: { error: 'has an error here' } })
  })
})
