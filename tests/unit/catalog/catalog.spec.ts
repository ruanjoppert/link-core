import { Catalog } from '../../../src/packages/catalog/catalog'
import { Endpoint } from '../../../src/packages/endpoint'

describe('Catalog', () => {
  const endpoint = new Endpoint({
    address: { domain: 'users', path: 'findOne', version: '0.1.0' },
    local: true,
    props: { id: 'string|max:10' }
  })

  describe('Target Strings', () => {
    it('should parser a target string into a address (v1 users.findOne)', () => {
      expect(
        Catalog.parseTargetString('v1 users.findOne')
      ).toEqual({ domain: 'users', path: 'findOne', version: '1.0.0' })
    })

    it('should parser a target string into a address (users.findOne)', () => {
      expect(
        Catalog.parseTargetString('users.findOne')
      ).toEqual({ domain: 'users', path: 'findOne', version: undefined })
    })

    it('should parser a target string into a address (v1.5 users)', () => {
      expect(
        Catalog.parseTargetString('v1.5 users')
      ).toEqual({ domain: 'users', path: undefined, version: '1.5.0' })
    })

    it('should parser a target string into a address (users)', () => {
      expect(
        Catalog.parseTargetString('users')
      ).toEqual({ domain: 'users', path: undefined, version: undefined })
    })

    it('should parser a target string into a address (v1.5.5 users)', () => {
      expect(
        Catalog.parseTargetString('v1.5.5 users')
      ).toEqual({ domain: 'users', path: undefined, version: '1.5.5' })
    })

    it('should be create a target string from endpoint', () => {
      expect(Catalog.mountTargetString(new Endpoint({
        address: { domain: 'users', path: 'findOne', version: '0.1.0' }
      }))).toBe('v0.1.0 users.findOne')

      expect(Catalog.mountTargetString(new Endpoint({
        address: { domain: 'users', version: '0.1.0' }
      }))).toBe('v0.1.0 users')

      expect(Catalog.mountTargetString(new Endpoint({
        address: { domain: 'users' }
      }))).toBe('users')

      expect(Catalog.mountTargetString(new Endpoint({
        address: { domain: 'users', path: 'findOne' }
      }))).toBe('users.findOne')
    })
  })

  it('should return an error if endpoint in invalid or not provided', () => {
    const catalog = new Catalog()

    expect(catalog.add()).toBeInstanceOf(Error)
  })

  describe('Endpoint operations with endpoint', () => {
    it('should return the endpoint if added', () => {
      const catalog = new Catalog()

      expect(catalog.add(endpoint)).toEqual(endpoint)
    })

    it('should replace a older endpoint', () => {
      const catalog = new Catalog()
      const endpoint2 = { ...endpoint }

      catalog.add(endpoint)
      catalog.add(endpoint2)

      expect(catalog.length).toBe(1)
      expect(catalog.getAll[0]).toEqual(endpoint2)
    })

    it('should not add a endpoint if already exists', () => {
      const catalog = new Catalog()

      catalog.add(endpoint)
      catalog.add(endpoint)

      expect(catalog.length).toBe(1)
    })

    it('should remove an endpoint from catalog by endpoint', () => {
      const catalog = new Catalog()
      catalog.add(endpoint)

      expect(catalog.remove(endpoint)).toBeTruthy()
      expect(catalog.length).toBe(0)
    })
  })

  describe('Endpoint operations with target', () => {
    it('should remove an endpoint from catalog by endpoint', () => {
      const catalog = new Catalog()
      const remove = catalog.remove(endpoint)

      expect(remove).toBeTruthy()
      expect(catalog.length).toBe(0)
    })

    it('should return an error if try remove a endpoint with a incomplete target string', () => {
      const catalog = new Catalog()
      catalog.add(endpoint)

      expect(catalog.remove('users.findOne')).toBeInstanceOf(Error)
      expect(catalog.length).toBe(1)
    })

    it('should remove an endpoint from catalog by target string', () => {
      const catalog = new Catalog()
      catalog.add(endpoint)

      expect(catalog.remove('v0.1.0 users.findOne')).toBeTruthy()
      expect(catalog.length).toBe(0)
    })
  })
})
