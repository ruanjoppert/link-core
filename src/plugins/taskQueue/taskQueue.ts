import { mergeObject } from '../../utils'
import { Broker } from '../../core/broker'

const nextTimeFunction = (attempts: number, maxAttempts: number, delay: number, critical: number) => attempts * ((critical - delay) / maxAttempts) + delay

/**
 * Default options
 */
const options = {
  attempts: 5,
  critical: 60 * 60 * 1000,
  delay: 10 * 1000,
  nextTimeFunction
}

export class TaskQueue {
  public tasks: any[] = []
  private broker: Broker

  constructor (broker: any) {
    this.broker = broker
  }

  /**
   * Create a new task
   */
  create (message: any, config: any = {}, flow: any, reply: any) {
    if (!message) {
      return false
    }

    const task: any = {}
    const id = message?.head?.id || Date.now()

    task.options = mergeObject(options, config)

    const exists = this.find(id)

    if (exists) {
      return exists
    }

    const { attempts, critical, delay, nextTimeFunction } = task.options

    task.id = id
    task.attempts = 0
    task.message = message
    task.critical = critical
    task.flow = flow
    task.retry = nextTimeFunction(0, attempts, delay, critical)
    task.created = Date.now()
    task.reply = reply

    this.broker.logger.info(`Task created (${task.id})`, { label: 'task', task })

    this.tasks.push(task)
  }

  /**
   * Increase attempts
   */
  increase (id: string) {
    const task = this.find(id)

    if (!task) {
      return
    }

    const { attempts, critical, delay, nextTimeFunction } = task.options

    task.attempts++
    task.retry = nextTimeFunction(task.attempts, attempts, delay, critical)
  }

  /**
   * Remove a task
   */
  remove (id: string) {
    const task = this.find(id)
    const index = this.tasks.indexOf(task)

    if (index > -1) {
      this.tasks.splice(index, 1)
    }
  }

  /**
   * Find task
   */
  find (id: string) {
    return this.tasks.find((task: any) => task.id === id)
  }

  /**
   * Ends the message with success
   */
  done (id: string) {
    const task = this.find(id)

    if (task.flow && task.flow.onSuccess) {
      task.flow.onSuccess(task, task.reply)
    }

    if (task.reply.end) {
      task.reply.end()
    }

    this.broker.logger.info(`Task completed after ${task.attempts}`, { label: 'task', task })

    this.remove(id)
  }

  /**
   * Ends the message with error
   */
  fail (id: string) {
    const task = this.find(id)

    if (task.flow && task.flow.onFail) {
      task.flow.onFail(task, task.reply)
    }

    if (task.reply.end) {
      task.reply.end()
    }

    this.broker.logger.info(`Task fail after ${task.attempts}`, { label: 'task', task })

    this.remove(id)
  }

  // ==========================================================================

  /**
   * Start the queue
   */
  start (interval = 30) {
    const start = () => {
      for (const key in this.tasks) {
        const task = this.tasks[key]

        this.work(task)
      }
    }

    setInterval(() => start(), interval * 1000)
  }

  /**
   * Work on task
   */
  work (task: any) {
    if (Date.now() <= task.created + task.retry) {
      return
    }

    const { attempts } = task.options
    const { reply } = task

    const queueReply = Object.assign(Object.create(Object.getPrototypeOf(reply)), reply)

    queueReply.enqueue = async () => {
      if (task.flow && task.flow.onError) {
        task.flow.onError(task, reply)
      }

      this.increase(task.id)
    }

    queueReply.error = async () => this.fail(task.id)
    queueReply.send = async () => this.done(task.id)

    task.flow.handler(task.message, queueReply)

    if (task.attempts >= attempts) {
      this.fail(task.id)
    }
  }
}
