import { TaskQueue } from './taskQueue'
import { Broker } from '../../core/broker'
import { Reply } from '../../packages/reply'
import { mergeObject } from '../../utils'

/**
 * Default options
 */
const defaultOptions = {
  interval: 30
}

export const taskQueue = (config: any = {}) => (broker: Broker) => {
  const taskQueue = new TaskQueue(broker)
  const options = mergeObject(defaultOptions, config)

  broker.taskQueue = taskQueue

  broker.hooks.register('callResource', async ({ handler, inbox, data }, next) => {
    if (data.message.type !== 'REQ') {
      next()

      return handler()
    }

    const { id } = data.message.head
    const { stream, group } = inbox

    if (inbox.resource) {
      const ack = () => broker.transporter.ack(stream, group, id)
      const reply = new Reply(broker, data.message, ack)

      /**
       * Add enqueue method
       */
      reply.enqueue = (response: any) => {
        if (response) {
          reply.send(response, false)
        }

        const messageTaskOptions = data.message.body?.options?.queue || {}
        const serviceTaskOptions = inbox.resource?.config?.queue || {}

        const taskOptions = mergeObject(serviceTaskOptions, messageTaskOptions)

        taskQueue.create(data.message, taskOptions, inbox.resource, reply)
      }

      await inbox.resource.handler(data.message, reply)
    } else {
      broker.transporter.ack(stream, group, data.message.head.id)
    }

    next()
  })

  broker.hooks.register('afterStart', async (data, next) => {
    taskQueue.start(options.interval || 30)

    broker.logger.info(`Plugin TaskQueue started (${options.interval})`, { label: 'task' })
    next()
  })
}
