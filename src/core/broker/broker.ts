import { serializer, Packages as SerializerPackages } from '../../packages/serializer'
import { transporter, Packages as TransporterPackages } from '../../packages/transporter'

import { IBrokerOptions, Inbox } from './broker.interface'

import { getNodeID, mergeObject, to, DeferredPromise, delay } from '../../utils'
import { Catalog } from '../../packages/catalog'
import { Middleware, nextType, endType } from '../../packages/middleware'
import { Hooks } from '../../packages/hooks'
import { Endpoint } from '../../packages/endpoint'
import { Reply } from '../../packages/reply'
import { IMessageType, IMessageBody, IMessageHead, Message, IMessage } from '../../packages/message'
import { PropsValidator } from '../../packages/validator'
import { logger } from '../../common/logger'

/**
 * Default options
 */
const options: IBrokerOptions = {
  namespace: '',
  nodeId: getNodeID(),

  transporter: {
    adapter: 'Redis',
    options: {
      host: '127.0.0.1',
      port: 6379
    }
  },

  serializer: {
    format: 'JSON',
    force: false
  },

  logLevel: 'info'
}

export class Broker {
  // Accepts additions of methods and or properties
  [index: string]: any

  public options: IBrokerOptions

  public transporter: InstanceType<TransporterPackages[this['options']['transporter']['adapter']]>
  private serializer: InstanceType<SerializerPackages[this['options']['serializer']['format']]>

  public catalog: Catalog
  public hooks: Hooks
  private inboxes: Inbox[] = []

  private middlewares: { [index: string]: Middleware } = {}

  public logger = logger

  constructor (config: Partial<IBrokerOptions> = {}) {
    this.options = mergeObject(options, config)

    /**
     * Resolves adapters
     */
    this.transporter = transporter(this.options.transporter.adapter, this.options.transporter.options) as any
    this.serializer = serializer(this.options.serializer.format) as any

    /**
     * Packages
     */
    this.catalog = new Catalog()
    this.hooks = new Hooks()

    /**
     * Default middlewares
     */
    this.middlewares.receiving = new Middleware()
    this.middlewares.sending = new Middleware()
    this.middlewares.errorOnReceiving = new Middleware()
    this.middlewares.errorOnSending = new Middleware()

    this.logger.level = this.options.logLevel
  }

  /**
   * Register an endpoint to listen for new messages
   * @param endpoint
   * @param resource
   */
  async listen (endpoint: Endpoint, options?: { group?: string, consumer?: string, initialId?: string }, resource?: any) {
    const stream = `${this.options.namespace}:${Catalog.mountTargetString(endpoint)}`
    const group = options?.group || stream
    const consumer = options?.consumer || this.options.nodeId
    const initialId = options?.initialId || '0'

    await this.hooks.call('beforeStartListen', { stream, endpoint, resource })

    this.logger.debug(`Start registering a listener on ${stream}`, { endpoint, resource })

    /**
     * Simple validation
     */
    if (!endpoint || !endpoint.address?.domain) {
      await this.hooks.call('invalidEndpointOnListen', { endpoint })

      this.logger.warn(`Target ${endpoint.address?.domain} invalid`, { label: 'invalid', endpoint })

      return Promise.reject(new Error('Invalid endpoint'))
    }

    /**
     * Validate props
     */
    if (endpoint.props && Object.keys(endpoint.props).length > 0 && endpoint.props.constructor === Object) {
      const v = new PropsValidator()
      const validate = v.compile(endpoint.props)

      if (validate !== true) {
        return Promise.reject(validate)
      }
    }

    /**
     * Register a new endpoint
     */
    endpoint.nodes = [this.options.nodeId]
    this.register(endpoint)

    /**
     * Create a subscribe function that will be called when broker start
     */
    const subscribe = async (inbox: Inbox) => {
      const { stream, group, consumer } = inbox

      /**
       * Create stream
       */
      if (await inbox.transporter.createStream(stream, group) !== true) {
        await this.hooks.call('errorOnCreateStream', { stream, group })

        this.logger.error(`Error on create stream :${stream} group: ${group}`, { label: 'error', stream, group })

        return Promise.reject('Impossible create stream')
      }

      /**
       * Separate function to allow the loop
       */
      const read: any = async () => {
        let [errorOnRead, preMessage] = await to(inbox.transporter.read(stream, consumer, group))

        /**
         * If an error occurs when read a message
         */
        if (errorOnRead) {
          return Promise.reject(errorOnRead)
        }

        /**
         * Ignore messages sent by this same broker
         */
        if (preMessage.head.sender === `${this.options.namespace}:${this.options.nodeId}`) {
          this.transporter.ack(stream, group, preMessage.head.id)

          return read()
        }

        /**
         * Deserialize message
         */
        preMessage = this.serializer.deserialize(preMessage)

        /**
         * Start receiving middleware
         */
        const [errorOnProcess, data] = await to(this.middlewares.receiving.process({ message: preMessage, stream, group }))

        /**
         * Error handling
         */
        if (errorOnProcess) {
          const [errorMiddleware] = await to(this.middlewares.errorOnReceiving.process(errorOnProcess))

          return Promise.reject(errorMiddleware)
        }

        await this.hooks.call('afterReceiveMessage', { message: data.message })

        this.logger.debug(`${data.message.type} message receive from ${data.message.head.sender}`, { msg: data.message, label: 'messageReceive' })

        /**
         * Call the handler callback
         */
        await this.hooks.call('callResource', { inbox, data }, async () => {
          if (inbox.resource) {
            const { id } = data.message.head

            const ack = () => this.transporter.ack(stream, group, id)
            const reply = new Reply(this, data.message, ack)

            await inbox.resource.handler(data.message, reply)
          } else {
            this.transporter.ack(stream, group, data.message.head.id)
          }
        })

        read()
      }

      await this.hooks.call('beforeSubscribe', { stream, group, consumer: this.options.nodeId })

      this.logger.info(`Subscribe at: ${stream}`, { stream, group, consumer })

      // start listen
      const [error] = read()

      /**
       * If has an error on receive message
       */
      if (error) {
        return Promise.reject('Error on read message')
      }
    }

    /**
     * Create a new inbox
     */
    const inbox : Inbox = {
      transporter: transporter(this.options.transporter.adapter, { ...this.options.transporter.options, initialId  }),
      resource,
      subscribe,
      stream,
      group,
      consumer
    }

    this.inboxes.push(inbox)
  }

  /**
   * Publish a message into a channel
   * @param type message type
   * @param target stream or target string
   * @param body body
   */
  public async publish <T extends IMessageType> (type: T, target: string, body: IMessageBody<T>, partialHead: Partial<IMessageHead> = {}) : Promise<string | Error> {
    if (!this.transporter.isConnected) {
      return Promise.reject('Transporter is not connect, call start() first and wait until complete')
    }

    if (!target) {
      return Promise.reject('Impossible to send a message to stream')
    }

    /**
     * If target ends with '!' the message will send even a endpoint is not found
     */
    const exact = !target.endsWith('!')

    /**
     * Remove, if exists '!' from target
     */
    if (!exact) target = target.slice(0, -1)

    /**
     * Find endpoint
     */
    const endpoint = this.catalog.find(target)

    if (!endpoint && exact) {
      await this.hooks.call('sendingTargetNotFound', { target })

      this.logger.warn(`Target ${target} not found`, { label: 'invalid', target })

      return Promise.reject('Target not found, try the complete name with ! in the end, eg: v1.0.0 users.create!')
    }

    /**
     * Props Validation
     */
    if (type === 'REQ' && endpoint) {
      const v = new PropsValidator()
      const { params } = body as any

      const schema = endpoint.props
      const valid = v.validate(schema, params)

      if (valid !== true) {
        this.hooks.call('onInvalidProps', { valid, params, schema })

        this.logger.warn(`Invalid props, ${valid.message}`, { label: 'invalid', erros: valid.errors, params, schema })

        return Promise.reject(`Invalid props, ${valid.message}`)
      }
    }

    const stream = endpoint ? `${this.options.namespace}:${Catalog.mountTargetString(endpoint)}` : target
    const serializedBody = this.serializer.serialize(body) as any

    const head = mergeObject({
      format: this.options.serializer.format,
      sender: `${this.options.namespace}:${this.options.nodeId}`,
      receiver: stream
    }, partialHead)
    const message = new Message(type, serializedBody, head)

    const [error, data] = await to(this.middlewares.sending.process({ message, stream }))

    if (error) {
      await this.hooks.call('sendingErrorOnProcess', { target, error })

      this.logger.warn(`Target ${target} not found`, { label: 'invalid', target })

      return Promise.reject('Error on sending message')
    }

    const messageId: string = await this.transporter.send(data.message, stream)

    await this.hooks.call('afterSendMessage', { message: { type, body, head }, messageId, stream })

    this.logger.debug(`${message.type} Message send to ${stream}`, { label: 'messageSend', msg: message, stream, messageId })

    return messageId
  }

  /**
   * Send request and wait a response
   * @param to
   * @param params
   * @param options
   */
  async call (target: string, params?: any, options?: any) : Promise<IMessage | Error> {
    const [error, id] = await to(this.publish('REQ', target, { params, options }))
    const timeout = options?.timeout || 5000

    if (error) {
      return new Error(error)
    }

    this.pendingRequests[id] = new DeferredPromise()

    return new Promise((resolve, reject) => {
      let hasTimeout = false
      let resolved = false

      /**
       * Set timeout
       */
      setTimeout(() => {
        hasTimeout = true

        if (resolved) return

        this.hooks.call('callTimeout', { id })

        this.logger.warn(`Call ${id} has timeout`, { label: 'callTimeout', id })

        reject('Timeout')
      }, timeout)

      this.pendingRequests[id].then((msg: any) => {
        if (hasTimeout) return

        resolve(msg)
        resolved = true

        this.hooks.call('beforeCallResolved', { msg })

        this.logger.info('Call successfully resolved', { label: 'callSuccess', msg })
      })
    })
  }

  /**
   * Request
   */
  async req (type: IMessageType = 'REQ', target: string, data: any = {}, options?: any) : Promise<IMessage | Error> {
    const [error, id] = await to(this.publish(type, target, data))
    const timeout = options?.timeout || 5000

    if (error) {
      return new Error(error)
    }

    this.pendingRequests[id] = new DeferredPromise()

    return new Promise((resolve, reject) => {
      let hasTimeout = false
      let resolved = false

      /**
       * Set timeout
       */
      setTimeout(() => {
        hasTimeout = true

        if (resolved) return

        this.hooks.call('callTimeout', { id })

        this.logger.warn(`Call ${id} has timeout`, { label: 'callTimeout', id })

        reject('Timeout')
      }, timeout)

      this.pendingRequests[id].then((msg: any) => {
        if (hasTimeout) return

        resolve(msg)
        resolved = true

        this.hooks.call('beforeCallResolved', { msg })

        // this.logger.info('Call successfully resolved', { label: 'callSuccess', msg })
      })
    })
  }

  /**
   * Start the broker
   */
  async start () {
    await this.hooks.call('beforeStart', { broker: this })

    this.logger.info('Starting broker ...', { options: this.options })

    this.registerCommands()

    Broker.middlewareResolvePendingRequest(this)
    Broker.middlewareCommandMessage(this)

    /**
     * Listen this broker
     */
    await this.listen({
      address: { domain: '$all' },
      local: true,
      type: 'private',
      nodes: [this.options.nodeId]
    }, { group: this.options.nodeId, consumer: this.options.nodeId, initialId: '>' })

    await this.listen({
      address: { domain: this.options.nodeId },
      local: true,
      type: 'private',
      nodes: [this.options.nodeId]
    }, { group: this.options.nodeId, consumer: this.options.nodeId })

    /**
     * Connect transporter
     */
    const [error, connectTransporter] = await to(this.hooks.call('connectTransporter', { transporter: this.transporter }, async () => {
      return this.transporter.connect()
    }))

    if (!connectTransporter) {
      await this.hooks.call('impossibleConnectTransporter', { error })

      this.logger.error('Error on connect to transporter', { label: 'criticalError', error })

      return new Error('Impossible to conect on transporter')
    }

    /**
     * Loop the inbox
     */
    for (const key in this.inboxes) {
      const inbox = this.inboxes[key]

      // Connect inbox transporter
      const [, connectInbox] = await to(this.hooks.call('connectInbox', { inbox }, async () => {
        return inbox.transporter.connect()
      }))

      if (!connectInbox) {
        return new Error('Impossible to conect on inbox')
      }

      // Subscribe the streams
      const [error] = await to(inbox.subscribe(inbox))

      if (error) {
        await this.hooks.call('impossibleCreateInbox', { inbox, error })

        this.logger.error('Error create inbox', { label: 'error' })

        return new Error('Error on start inbox')
      }
    }

    /**
     * Send INFO message to all brokers
     */
    await Broker.scoutInfo(this)

    await this.hooks.call('afterStart', { broker: this })

    this.logger.info('Broker started', { label: 'started', options: this.options })

    // Wait info response
    await delay(1000)
  }

  // ==========================================================================
  // Extends and functionalities
  // ==========================================================================

  /**
   * Register an endpoint array or a service into broker
   */
  async register (endpointOrService: any) {
    if (!endpointOrService) {
      return false
    }

    /**
     * If is a service interface
     */
    if (endpointOrService.service) {
      // Register one endpoint for action
      endpointOrService.actions.forEach(async (action: any) => {
        const [error] = await to(this.listen(action.endpoint, {}, action.resource))

        if (error) {
          this.hooks.call('errorOnRegisterService', { action, error })

          this.logger.error(`Error on register service ${action.endpoint.address.domain}.${action.endpoint.address.path}, ${error.message || ''}`, { label: 'error', action, error })
        } else {
          this.hooks.call('onRegisterService', { action })
        }
      })

      return true
    }

    /**
     * If is a endpoint
     */
    const endpoints = Array.isArray(endpointOrService) ? endpointOrService : [endpointOrService]

    endpoints.forEach((endpoint: Endpoint) => {
      if (!endpoint.address) {
        return
      }

      this.hooks.call('beforeRegisterEndpoint', { endpoint })
      this.catalog.add(endpoint)
      this.logger.debug(`Register endpoint ${endpoint.address.domain} total: ${this.catalog.length}`)
      this.hooks.call('afterRegisterEndpoint', { endpoint })
    })
  }

  /**
   * Add middleware function
   */
  use (middlewares: { [index: string]: (data: any, next: nextType, end: endType) => void }) {
    for (const middlewareName in middlewares) {
      const middleware = middlewares[middlewareName]

      if (this.middlewares[middlewareName]) {
        this.middlewares[middlewareName].use(middleware)
      }
    }
  }

  /**
   * Extend broker and allow build plugins
   */
  extend (plugin: any) {
    plugin(this)
  }

  // ==========================================================================
  // Abstract functions
  // ==========================================================================

  /**
   * Send initial info message to all brokers and register external endpoints
   * @param broker
   */
  static async scoutInfo (broker: Broker) {
    // Create a copy of endpoints
    let endpoints = JSON.parse(JSON.stringify(broker.catalog.getAll))

    // Get only endpoints public and local
    endpoints = endpoints.filter((ep: Endpoint) => ep.local && ep.type === 'public')

    // Send message
    const [error] = await to(broker.publish('INFO', '$all', { endpoints }))

    if (error) {
      broker.hooks.call('infoImpossibleSendMessage', { error, endpoints })

      broker.logger.warn('Could not send info message', { label: 'invalid', error, endpoints })
    }

    /**
     * Register middleware that will interpret INFO messages and respond with own information
     */
    broker.use({
      receiving: async ({ message }, next) => {
        const { type, head } = message

        if (type !== 'INFO') {
          return next()
        }

        broker.hooks.call('onMessageInfoReceive', { message })

        if (message.body.endpoints) {
          const endpoints = message.body.endpoints.map((ep: Endpoint) => { ep.local = false; return ep })
            .map((ep: Endpoint) => { if (!ep.nodes) { ep.nodes = [message.head.sender] } return ep })

          await broker.register(endpoints)
        }

        // If the message is not a response will respond with own information
        if (!head.answering) {
          await broker.publish('INFO', `${head.sender}!`, { endpoints }, { answering: head.id })
        }

        next()
      }
    })
  }

  /**
   * Register commands
   * @param broker
   */
  registerCommands () {
    this.hooks.register('catalog.removeByNode', ({ node }, next) => {
      if (node) {
        this.catalog.removeByNode(node)
      }

      next()
    })
  }

  // ==========================================================================
  // Middlewares functions
  // ==========================================================================

  /**
   * Middleare responsable for intercepting response messages and resolving pending requests
   * @param broker
   */
  static middlewareResolvePendingRequest (broker: Broker) {
    broker.pendingRequests = {}

    broker.use({
      receiving: ({ message, stream, group }: any, next: any) => {
        const { type, head } = message

        if (head.answering && broker.pendingRequests[head.answering]) {
          broker.pendingRequests[head.answering].resolve(message)
        }

        next()
      }
    })
  }

  /**
   * Middleare responsable for intercepting CMD messages and run
   * @param broker
   */
  static middlewareCommandMessage (broker: Broker) {
    broker.use({
      receiving: ({ message, stream, group }: any, next: any) => {
        const { type, body } = message

        if (type !== 'CMD') {
          return next()
        }

        broker.hooks.call(body.cmd, body.params)

        next()
      }
    })
  }
}
