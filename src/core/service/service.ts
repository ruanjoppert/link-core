import { Action } from './service.interface'
import { Endpoint } from '../../packages/endpoint'

/**
 * Start a new service
 */
export class Service {
  public readonly service: string
  public readonly version: string
  private actions: [ { resource: any, endpoint: Endpoint }? ]

  /**
   * Create an instance of Service
   */
  constructor (
    object: { service: Service['service'], version: Service['version']} & Partial<Service>
  ) {
    this.service = object.service
    this.version = object.version
    this.actions = []
  }

  /**
   * Register new action into service
   */
  action (action: Action) {
    const { name, props, config, ...resource } = action

    const endpoint = new Endpoint({
      address: { domain: this.service, path: name, version: this.version },
      local: true,
      type: 'public',
      props,
      config
    })

    this.actions.push({ endpoint, resource })
  }
}

export default Service
