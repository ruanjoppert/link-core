import { Reply } from '../../packages/reply'

type actionHandlers = (data: any, reply: Reply) => any

interface config {
  http?: {
    method: string
    group?: string
    params?: string
    path?: string
  }

  auth?: boolean | string

  queue?: {
    attempts?: number
    critical?: number
    delay?: number
  },

  [index: string]: any
}

export interface Action {
  name: string
  config: config
  handler: (data: any, reply: Reply) => any

  [index: string]: actionHandlers | any
}
