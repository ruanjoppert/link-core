/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export const isObject = (item: object) : boolean => (item && typeof item === 'object' && !Array.isArray(item) && !Buffer.isBuffer(item))

/**
 * Camelize a string
 * @param str
 */
export const camalize = (str: string) : string => str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase())

/**
 * Rurns a deep object into a shallow
 * @param object
 * @param name
 */
export const shallow = <T extends object>(object: T, name?: string|object) : T => {
  if (!isObject(object)) {
    return object
  }

  return Object.keys(object).reduce((prev, key) => {
    const value = object[key as keyof object]

    if (isObject(value)) {
      const arrName : object = name ? [...name, key] : [key]
      const teste = shallow(value, arrName)

      return Object.assign(prev, teste)
    }

    const arrName = name ? [...name, key] : [key]
    const keyName = arrName.length > 1 ? camalize(arrName.join(' ')) : arrName.join(' ')

    return Object.assign(prev, { [keyName]: value })
  }, {}) as any
}

/**
 * Delay for Promises
 *
 * @param {any} ms
 * @returns
 */
export const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))

/**
 * Slipt an array in multiple array with N size
 * @param {*} array
 * @param {*} size
 */
export const chunkArray = <T extends Array<any>> (array: T, size: number) => array.reduce((all, one, i) => {
  const ch = Math.floor(i / size)
  all[ch] = [].concat((all[ch] || []), one)
  return all
}, [])

/**
 * Transform a array of key value array into object
 * @param entries
 */
export const fromEntries = <T>(entries: Array<[any, any]>): any => {
  return entries.reduce(
    (acc, [key, value]) => ({ ...acc, [key]: value }),
    {}
  )
}

/**
 * Merge two object into one deeply
 * @param target
 * @param source
 */
export function mergeObject (target: any, source: any) {
  if (!isObject(target) || !isObject(source)) {
    return source
  }

  Object.keys(source).forEach((key) => {
    const targetValue = target[key]
    const sourceValue = source[key]

    if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
      target[key] = targetValue.concat(sourceValue)
    } else if (isObject(targetValue) && isObject(sourceValue)) {
      target[key] = mergeObject({ ...targetValue }, sourceValue)
    } else {
      target[key] = sourceValue
    }
  })

  return target
}

/**
 * Compare values into array and return a index ordered
 * @param array
 * @param value
 * @param comparator
 */
export const lowerBound = <T>(array: readonly T[], value: T, comparator: (a: T, b: T) => number): number => {
  let first = 0
  let count = array.length

  while (count > 0) {
    const step = (count / 2) | 0
    let it = first + step

    if (comparator(array[it], value) <= 0) {
      first = ++it
      count -= step + 1
    } else {
      count = step
    }
  }

  return first
}

export const parseVersion = (versionString: string) => {
  const [major, minor, patch] = versionString.split('.')

  return {
    major: major || '',
    minor: minor || '',
    patch: patch || ''
  }
}

export const compareVersion = (target: any, version: any) => {
  return Object.keys(target).reduce((prev, key) => {
    const targetValue = target[key]
    const versionValue = version[key]

    if (prev && targetValue && targetValue !== '') {
      return targetValue === versionValue
    }

    return prev
  }, true)
}
