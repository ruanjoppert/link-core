export class DeferredPromise {
  private promise: Promise<any>

  public resolve!: Function
  public reject!: Function
  public then: Function
  public catch: Function

  constructor () {
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve
      this.reject = reject
    })

    // bind `then` and `catch` to implement the same interface as Promise
    this.then = this.promise.then.bind(this.promise)
    this.catch = this.promise.catch.bind(this.promise)
    // this[Symbol.toStringTag] = 'Promise'
  }
}
