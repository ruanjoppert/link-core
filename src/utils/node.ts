import os from 'os'

/**
 * Get default NodeID (computerName)
 *
 * @returns
 */
export const getNodeID = () => `${os.hostname().toLowerCase()}-${process.pid}`
