/**
 * Short hand for errors
 * @param {function} fn Function to be handler
 * @returns {any}
 */
export const to = (fn: any) => {
  if (fn && typeof fn.then === 'function') {
    return fn
      .then((data: any) => ([null, data]))
      .catch((error: any) => [error, null])
  }

  if (fn instanceof Error) return [fn, null]

  // If fn is a function
  if (typeof fn === 'function') {
    return (...args: any) => {
      try {
        return [null, fn(...args)]
      } catch (error) {
        return [error, null]
      }
    }
  }

  return [null, fn]
}
