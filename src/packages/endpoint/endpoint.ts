import { IEndpointAdress } from './endpoint.interface'

/**
 * Endpoint
 */
export class Endpoint {
  public address: IEndpointAdress
  public local: boolean
  public type?: 'public' | 'private'
  public props?: any
  public config?: any
  public nodes: string[]

  constructor (endpoint: {
    address: IEndpointAdress,
    local?: boolean,
    type?: 'public' | 'private',
    props?: any
    config?: any,
    nodes?: any
  }) {
    this.address = endpoint?.address
    this.local = endpoint?.local || false
    this.type = endpoint?.type || 'public'
    this.props = endpoint?.props || {}
    this.config = endpoint?.config || {}
    this.nodes = endpoint?.nodes || []
  }
}
