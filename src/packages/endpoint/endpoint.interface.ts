export type IEndpointAdress = { domain: string, path?: string, version?: string }
