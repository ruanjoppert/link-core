import { BaseSerializer } from '../base'
import { Message, IMessage } from '../../message'

/**
 * Serializer
 *
 * @class Serializer
 * @param {Packet}
 */
class JSONSerializer extends BaseSerializer {
  constructor () {
    super()

    this.format = 'JSON'
  }

  /**
   * Serializer a JS object to Buffer
   */
  serialize <T> (message: T): T extends IMessage ? IMessage : any {
    if (message instanceof Message) {
      const seriazedMessage = { ...message }
      seriazedMessage.body = Buffer.from(JSON.stringify(seriazedMessage.body || {}))

      return seriazedMessage as any
    }

    return Buffer.from(JSON.stringify(message || {})) as any
  }

  /**
   * Deserialize Buffer to JS object
   */
  deserialize <T extends IMessage> (message : T): T {
    if (message.body) {
      message.body = JSON.parse(message.body)

      return message as any
    }

    return JSON.parse(message as any)
  }
}

export default JSONSerializer
