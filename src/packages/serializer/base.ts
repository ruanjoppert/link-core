/**
 * Base Serializer
 */
export abstract class BaseSerializer {
  public format: string = ''

  abstract serialize (object: any): any
  abstract deserialize (buffer: any): any
}
