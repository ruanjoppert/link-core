import JSON from './adapters/JSON'

export const Packages = {
  JSON
}

export type Packages = typeof Packages

export const serializer = <T extends keyof Packages> (adapter: T) : T extends keyof Packages ? InstanceType<Packages[T]> : Error => {
  if (!Object.keys(Packages).includes(adapter)) {
    return new Error('Seralizer package not found') as any
  }

  return new Packages[adapter]() as any
}

export * from './base'
