import { Middleware } from '../middleware'

export class Hooks {
  private hooks: any = {}

  /**
   * Register hook
   */
  register (hookName: string, hook: (data: { [index: string] : any }, next: any) => any) {
    if (!hookName || !hook) {
      return false
    }

    if (!this.hooks[hookName]) {
      this.hooks[hookName] = new Middleware()
    }

    this.hooks[hookName].use(hook)

    return true
  }

  /**
   * Call hook
   * @param hookName
   * @param data
   * @param handler
   */
  async call (hookName: string, data?: any, handler?: any) {
    let params: any = { ...data }
    const hook = await this.hooks[hookName]

    // If dont have hook but have a default handler
    if (!hook && handler) {
      return await handler(params)
    }

    // If dont have hook and default handler
    if (!hook && !handler) {
      return
    }

    // If have hook and handler pass handler in params
    if (hook && handler) {
      params = { handler, ...params }
    }

    const { handler: _, ...result } = await hook.process(params)

    return result
  }
}
