import Ajv from 'ajv'

export class PropsValidator {
  private validator: any

  constructor () {
    this.validator = new Ajv({ removeAdditional: false })
  }

  compile (schema: any) {
    try {
      const compile = this.validator.compile(schema)

      return true
    } catch (error) {
      return error
    }
  }

  validate (schema: any, params: any) {
    const valid = this.validator.validate(schema, params)

    if (!valid) {
      return {
        message: this.validator.errorsText(),
        errors: this.validator.errors
      }
    }

    return true
  }
}
