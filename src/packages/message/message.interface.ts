import { Endpoint } from '../endpoint'

/**
 * Message Type
 */
export type IMessageType = 'REQ' | 'RES' | 'INFO' | 'EVENT' | 'CMD'

/**
 * Body Message
 */
export type IMessageBody <T> = T extends 'REQ' ? IMessageBodyREQ
  : T extends 'RES' ? IMessageBodyRES
  : T extends 'CMD' ? IMessageBodyCMD
  : T extends 'INFO' ? IMessageBodyINFO
  : never

/**
 * Message Head
 */
export interface IMessageHead {
  format: string // Format that the message was serialized | JSON
  sender: string // Fullname of who is sending the message | cappel-0001
  receiver: string // Stream that will receive the message
  id?: string // Message id. It is optional because the id is generated after the message is created | 12345-0
  correlationId?: string // Id shared among all messages generated from a single message | 12345-0-0
  answering?: string // It is Id if the message is a response | 123-0
  accept?: string // Tells to receiver wich type of serialization is preferred | JSON,protobuf
  language?: string // Tells to receiver wich language is preferred | pt-br
  expires?: string // Timestap at in the end the message is ignored
}

export interface IMessage {
  type: IMessageType,
  body: any,
  head: IMessageHead
}
// ============================================================================

interface IMessageBodyREQ {
  params?: object
  options?: object
}

interface IMessageBodyRES {
  data?: object
  error?: boolean
}

interface IMessageBodyCMD {
  cmd: string
  params?: object
}

interface IMessageBodyINFO {
  endpoints?: Endpoint[]
}
