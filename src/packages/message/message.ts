import { IMessageType, IMessageBody, IMessageHead } from './message.interface'
import { shallow } from '../../utils'

export class Message <T extends IMessageType, U extends IMessageBody<T>, V extends IMessageHead> {
  constructor (
    public type: T,
    public body: U,
    public head: V
  ) {
    this.type = type || 'unknown'
    this.body = body || {}
    this.head = shallow(head)
  }

  /**
   * Parse a object into a message
   * @param object
   */
  static parse (object: any) {
    const { type, body, ...head } = object

    return new Message(type, body, head)
  }
}
