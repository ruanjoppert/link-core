import { IMessage } from '../message'
import { Broker } from '../../core/broker'
import { to } from '../../utils'

export class Reply {
  [index: string]: any

  constructor (
    private broker: Broker,
    private message: IMessage,
    private ack: Function
  ) {
    this.broker = broker
  }

  /**
   * Send a RES message for the sender
   * @param response
   */
  async send (response: any, finish = true) {
    // Send message
    const [error] = await to(this.broker.publish('RES', `${this.message.head.sender}!`, {
      data: response,
      error: false
    }, { answering: this.message.head.id }))

    // Call the ack
    if (finish) this.ack()

    // Error
    if (error) {
      console.log('error on send reply send')
    }
  }

  /**
   * Send a RES message for the sender with error
   * @param response
   */
  async error (response: any, finish = true) {
    // Send message
    const [error] = await to(this.broker.publish('RES', `${this.message.head.sender}!`, {
      data: response,
      error: true
    }, { answering: this.message.head.id }))

    // Call the ack
    if (finish) this.ack()

    // Error
    if (error) {
      console.log('error on send reply send')
    }
  }

  /**
   * Finish without send anything
   */
  end (finish = true) {
    this.ack()
  }
}
