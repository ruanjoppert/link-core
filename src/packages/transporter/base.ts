import { IMessage } from '../message'

/**
 * Base Transporter
 */
export abstract class BaseTransporter {
  public options: any
  public isConnected: boolean
  public client: any

  /**
   * Create an instance of BaseTransporter
   */
  constructor (options: any) {
    this.options = options
    this.isConnected = false
    this.client = null
  }

  abstract connect(): Promise<boolean | Error>
  abstract disconnect(): void
  abstract send(data: IMessage, channel: string): Promise<string>
  abstract read(channel: string, consumer: string, group: string): Promise<IMessage | Error>

  connected (): void {}
  disconnected () : void {}

  /**
   * Event hander for connected
   */
  onConnected (client: object) {
    this.isConnected = true
    this.client = client

    if (this.connected) {
      return this.connected()
    }

    return Promise.resolve()
  }

  /**
   * Event hander for disconnected
   */
  onDisconnected () {
    this.client = null
    this.isConnected = false

    if (this.disconnected) {
      return this.disconnected()
    }

    return Promise.resolve()
  }

  /**
   * Event handler for error
   */
  onError (error: string) {
    return new Error(error)
  }
}
