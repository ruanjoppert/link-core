import Redis from './adapters/redis'

export const Packages = {
  Redis
}

export type Packages = typeof Packages

export const transporter = <T extends keyof Packages> (adapter: T, options: object = {}) : T extends keyof Packages ? InstanceType<Packages[T]> : Error => {
  if (!Object.keys(Packages).includes(adapter)) {
    return new Error('Seralizer package not found') as any
  }

  return new Packages[adapter](options) as any
}

export * from './base'
