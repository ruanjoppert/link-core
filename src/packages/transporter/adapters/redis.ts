import { BaseTransporter } from '../base'
import { Message, IMessage } from '../../message'
import { to, isObject, chunkArray, fromEntries, DeferredPromise } from '../../../utils'

/**
 * Redis Transporter
 */
class RedisTransporter extends BaseTransporter {
  public client!: any

  private lastId: string

  /**
   * RedisTransporter
   * @param {*} options
   */
  constructor (options: any) {
    super({
      ...options,
      showFriendlyErrorStack: true
    })

    this.lastId = this.options.initialId || '0-0'
  }

  /**
   * Connect to server
   */
  async connect () {
    if (this.isConnected) return true

    const [error, IOredis] = to(require)('ioredis')

    if (error) {
      return Promise.reject(new Error('Package ioredis not found, please try install with yarn add ioredis and try again.'))
    }

    /**
     * Instance new client (used for future publish)
     */
    const client = new IOredis(this.options)

    const isConnected = new DeferredPromise()

    client.on('connect', () => {
      isConnected.resolve()
      this.onConnected(client)
    })

    client.on('close', () => this.onDisconnected())
    client.on('error', (err: string) => {
      isConnected.reject(err)
      this.onError(err)
    })

    const [connectionError] = await to(isConnected)

    if (connectionError) {
      return Promise.reject(new Error(connectionError))
    }

    return true
  }

  /**
   * Disconnect from server
   */
  disconnect () {
    if (this.client) {
      this.client.disconnect()
    }
  }

  /**
   * Publish a packet into a channel
   */
  async send (message: IMessage, stream: string) {
    if (!message || !stream) {
      return new Error('Missing params')
    }

    const { type, body, head } = message
    const messageToBeSend: any = { type, body, ...head }

    const arrMessage = Object.keys(messageToBeSend).reduce((prev: string[], key) => {
      const value = messageToBeSend[key]

      if (!value || isObject(value)) return prev

      prev.push(key, value)

      return prev
    }, [])

    if (arrMessage.indexOf('body') <= -1) {
      return new Error('The body is not present, it may not have been serialized correctly')
    }

    const [error, messageId] = await to(this.client.xadd(stream, '*', ...arrMessage))

    if (error) {
      return new Error(error)
    }

    return messageId
  }

  /**
   * Subscribe to a channel with group and consumer
   * @param {String} group
   * @param {String} consumer
   * @param {String} stream
   * @returns void
   */
  async read (stream: string, consumer: string, group: string) : Promise<IMessage | Error> {
    const [error, reply] = await to(this.client.xreadgroup('GROUP', group, consumer, 'COUNT', 1, 'BLOCK', '5000', 'STREAMS', stream, this.lastId))

    /**
     * If have error, throw
     */
    if (error) {
      console.log('error xreadgroup', error)
      return Promise.reject(error)
    }

    if (!reply) {
      return this.read(stream, consumer, group)
    }

    const results = reply[0][1]

    if (!results.length) {
      this.lastId = '>'

      return this.read(stream, consumer, group)
    }

    const [[id, message]] = results

    this.lastId = id

    const objectMessage = chunkArray(message, 2).reduce((obj: { [x: string]: any }, [key, value]: any) => {
      obj[key] = value

      return obj
    }, { id })

    // console.log('chegou uma mensagem aqui', objectMessage)

    return Message.parse(objectMessage)
  }

  /**
   * Mark a message as ack
   * @param stream
   * @param group
   * @param messageId
   */
  async ack (stream: string, group: string, messageId: string) {
    const [error, poa] = await to(this.client.xack(stream, group, messageId))

    if (error) {
      return false
    }

    return true
  }

  /**
   * Check if exists channel and/or group in channel
   * @param {*} channel
   * @param {*} group
   */
  async exists (stream: string, group?: string) {
    const [error, groups] = await to(this.client.xinfo('GROUPS', stream))

    if (!group) {
      return groups !== null
    }

    /**
     * Search in all groups
     */
    const inGroup = (error && error.message) === 'ERR no such key'
      ? false
      : groups.map((arr: any[]) => fromEntries(chunkArray(arr, 2)).name === group)
        .filter(Boolean).length

    return inGroup > 0
  }

  /**
   * Create one stream, group and enter on it
   * @param {String} channel
   * @param {String} group
   */
  async createStream (stream: string, group: string) : Promise<boolean | Error> {
    const exists = await this.exists(stream, group)

    if (exists) {
      return true
    }

    const [error] = await to(this.client.xgroup('CREATE', stream, group, '$', 'MKSTREAM'))

    if (error) {
      return new Error(error)
    }

    return true
  }
}

export default RedisTransporter
