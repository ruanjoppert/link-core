import { Endpoint, IEndpointAdress } from '../endpoint'
import { parseVersion, compareVersion } from '../../utils'

export class Catalog {
  private endpoints: Endpoint[] = []

  /**
   * Add a new endpoint
   * @param endpoint
   */
  add (endpoint: Endpoint) {
    if (!endpoint) return new Error('Missing params')

    const exists = this.find(endpoint)

    if (exists) {
      const nodes = exists.nodes || []

      this.remove(exists)

      if (!endpoint.nodes) {
        endpoint.nodes = []
      }

      endpoint.nodes = nodes.indexOf(endpoint.nodes[0]) <= -1 ? [...nodes, ...endpoint.nodes] : nodes
    }

    this.endpoints.push(endpoint)

    return endpoint
  }

  /**
   * Remove a new endpoint
   * @param endpoint
   */
  remove (endpoint: Endpoint | string) {
    let index: number = -1

    if (typeof endpoint === 'string') {
      const address = Catalog.parseTargetString(endpoint)

      if (!address.domain || !address.path || !address.version) {
        return new Error('Must be a complete target string to remove from catalog')
      }

      const find = this.endpoints.find((ep) => {
        if (
          address.domain === ep.address.domain &&
          address.path === ep.address.path &&
          address.version === ep.address.version
        ) {
          return true
        }
      })

      if (find) {
        index = this.endpoints.indexOf(find)
      }

      return false
    }

    index = this.endpoints.indexOf(endpoint)

    if (index > -1) {
      this.endpoints.splice(index, 1)
      return true
    }

    return false
  }

  /**
   *
   * @param targetString
   */
  removeByNode (nodeId: string) {
    this.endpoints.filter(ep => ep.nodes.indexOf(nodeId) > -1)
      .forEach(ep => ep.nodes.length > 1
        ? this.endpoints[this.endpoints.indexOf(ep)].nodes.splice(ep.nodes.indexOf(nodeId), 1)
        : this.endpoints.splice(this.endpoints.indexOf(ep), 1)
      )
  }

  /**
   * Find a endpoint
   * @param targetString
   */
  find (targetString: Endpoint | string) {
    const address = typeof targetString === 'string' ? Catalog.parseTargetString(targetString, false) : targetString.address

    const search = this.endpoints.filter(ep => {
      const find = ep.address.domain === address.domain && ep.address.path === address.path

      // not found
      if (!find) {
        return false
      }

      // find and version is not provided
      if (find && !address.version) {
        return true
      }

      // Find and version is provided
      if (find && address.version && ep.address.version) {
        return compareVersion(parseVersion(address.version), parseVersion(ep.address.version))
      }
    })
      .sort((a, b) => (a.address.version && b.address.version) ? (a.address.version < b.address.version) ? -1 : 1 : 1)
      .pop()

    if (!search) {
      return false
    }

    return search
  }

  /**
   * Return endpoint lengths
   */
  get length () {
    return this.endpoints.length
  }

  /**
   * Return endpoint lengths
   */
  get getAll () {
    return this.endpoints
  }

  // ==========================================================================

  /**
   * Parse a target string into adress of Endpoint
   * @param targetString
   * @param fullVersion
   */
  static parseTargetString (targetString: string, fullVersion = true) : IEndpointAdress {
    let version: any

    const [rest, namespace] = targetString.split(':').reverse()
    const [resource, v] = rest.split(' ').reverse()
    const [domain, path] = resource.split('.')

    if (v) {
      version = v.charAt(0) === 'v' ? v.substring(1) : v

      if (fullVersion) {
        version = parseVersion(version)
        version = Object.keys(version).reduce((prev: number[], key) => {
          const value = version[key] | 0

          prev.push(value)

          return prev
        }, []).join('.')
      }
    }

    return {
      domain,
      path,
      version
    }
  }

  /**
   * Mount a target string with Endpoint
   * @param endpoint
   */
  static mountTargetString (endpoint: Endpoint) {
    const version = endpoint?.address?.version ? `v${endpoint.address.version} ` : ''
    const domain = endpoint?.address?.domain
    const path = endpoint?.address?.path ? `.${endpoint.address.path}` : ''

    return `${version}${domain}${path}`
  }
}
