import { Pipeline } from './pipeline'
import { nextType, endType } from './middleware.interface'

export class Middleware {
  private middlewares: any[] = []

  use (middleware: (data: any, next: nextType, end: endType) => any) {
    if (middleware) {
      this.middlewares.push(middleware)
    }
  }

  async process (data: any) : Promise<any> {
    const pipeline = new Pipeline(this.middlewares, data)

    return await pipeline.dispatch()
  }
}
