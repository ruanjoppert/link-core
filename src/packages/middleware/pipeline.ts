export class Pipeline {
  public middlewares: any[]
  public data: any
  public finished: boolean

  constructor (middlewares: any, data: any) {
    this.middlewares = middlewares
    this.data = data

    this.finished = false
  }

  /**
   * @data stays in a closure here
   */
  async dispatch () {
    if (!this.middlewares.length) {
      return this.data
    }

    return new Promise(async (resolve, reject) => {
      // Itarator used to iterate on the middlewares list
      let iterator = 0

      // Select middlaware flow
      const middleware = this.middlewares

      // Adds a static end function
      const end = (data = undefined, isError = false) => {
        if (isError !== false) {
          reject({ data: this.data, error: data })
        }

        this.data = data && !isError ? data : this.data
        this.finished = true

        resolve(this.data)
      }

      if (iterator < middleware.length) {
        const firstMiddleware = middleware[iterator]
        /**
         * The next() is a wrap function, this way the middleware doesn't need to
         * pass data as parameter for the next middleware, the next() function
         * is responsible for getting it from the closure scope and
         * injecting for the next middleware
         */
        const next = async (data: any) => {
          iterator++

          this.data = data || this.data

          if (!this.finished && iterator < middleware.length) {
            const nextMiddleware = middleware[iterator]
            await nextMiddleware(this.data, next, end)
          } else {
            end()
          }
        }

        await firstMiddleware(this.data, next, end)
      }
    })
  }
}
