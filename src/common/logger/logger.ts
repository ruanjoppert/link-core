import { createLogger, format, transports } from 'winston'

const logError = format(info => {
  if (info.error && info.error.stack) {
    info.error = {
      message: info.error.message,
      stack: info.error.stack
    }
  }

  return info
})

export const logger = createLogger({
  level: 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    logError(),
    format.json()
  ),
  transports: [
    new transports.File({ filename: 'logs/error.log', level: 'error' }),
    new transports.File({ filename: 'logs/combined.log' })
  ]
})

const emoji = {
  error: '   ⊗  ',
  started: '    ☀  ',
  endpoint: '    ✒  ',
  invalid: '    ✖  ',
  messageSend: '   ✉ ↦',
  messageReceive: ' ↤ ✉  ',
  criticalError: '    ☠️  ',
  callSuccess: '    ✔  ',
  callTimeout: '    ☢  ',
  task: '    ⚒  '
}

const simpleFormat: any = (info: { label: keyof typeof emoji, [index: string]: any }) => `${info.timestamp} ${info.level} ${emoji[info.label] || ' '} ${info.message} (${info.ms})`

logger.add(new transports.File({
  filename: 'logs/simple.log',

  format: format.combine(
    format.timestamp(),
    format.ms(),
    format.printf(simpleFormat)
  )
}))

logger.add(new transports.Console({
  format: format.combine(
    format.timestamp({
      format: 'HH:mm:ss'
    }),
    format.simple(),
    format.ms(),
    format.colorize(),
    format.align(),
    format.printf(simpleFormat)
  )
}))
